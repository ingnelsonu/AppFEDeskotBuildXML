﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Referenciar y usar.
using System.IO;
using System.Windows.Forms;


namespace RULES
{
    public class clsBase
    {
        #region "Control de Errores"

        protected string FechaSQL(string Fecha)
        {
            try
            {
                string NewDate = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Fecha));
                return NewDate;
            }
            catch (Exception)
            {
                return Fecha;                
            }
        }

        protected string ReplaceDecimal(string Valor)
        {
            int IndexComa = 0;
            int IndexPunto = 0;

            try
            {
                IndexPunto = Valor.IndexOf(".");
                IndexComa = Valor.IndexOf(",");

                //Separador de miles =. decimales=,
                if (IndexPunto < IndexComa)
                {
                    Valor = Valor.Replace(".", "").Replace(",", ".");
                }
                //Separador de miles =, decimales=.
                else
                {
                    Valor = Valor.Replace(",", "");
                }

                return Valor;

            }
            catch (Exception)
            {
                return Valor;
            }
        }

        public void MensajeError(string Trasa,string IdPedido)
        {
            if (!(System.IO.Directory.Exists(Application.StartupPath + @"\\Errors\\")))
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\\Errors\\");
            }
            FileStream fs = new FileStream(Application.StartupPath + @"\\Errors\\Err_"+IdPedido+".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter s = new StreamWriter(fs);
            s.Close();
            fs.Close();
            FileStream fs1 = new FileStream(Application.StartupPath + @"\\Errors\\Err_" + IdPedido +".txt", FileMode.Append, FileAccess.Write);
            StreamWriter s1 = new StreamWriter(fs1);

            string[] partes = Trasa.Split(new char[] { '\n' });
            foreach (string item in partes)
            {
                //s1.WriteLine(Trasa);   
                s1.WriteLine(item);
            }         
            s1.Close();
            fs1.Close();
        }

        public void logIng(string SQLING, string IdPedido)
        {
            try
            {
                if (!(System.IO.Directory.Exists(Application.StartupPath + @"\\Errors\\")))
                {
                    System.IO.Directory.CreateDirectory(Application.StartupPath + @"\\Errors\\");
                }

                if (System.IO.File.Exists(Application.StartupPath + @"\\Errors\\LogSQLIng_" + IdPedido + ".txt"))
                    System.IO.File.Delete(Application.StartupPath + @"\\Errors\\LogSQLIng_" + IdPedido + ".txt");
          
                FileStream fs = new FileStream(Application.StartupPath + @"\\Errors\\LogSQLIng_" + IdPedido + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
                StreamWriter s = new StreamWriter(fs);
                s.Close();
                fs.Close();
                FileStream fs1 = new FileStream(Application.StartupPath + @"\\Errors\\LogSQLIng_" + IdPedido + ".txt", FileMode.Append, FileAccess.Write);
                StreamWriter s1 = new StreamWriter(fs1);

                string[] partes = SQLING.Split(new char[] { ' ' });
                foreach (string item in partes)
                {
                    //s1.WriteLine(Trasa);   
                    s1.WriteLine(item);
                }
                s1.Close();
                fs1.Close();
            }
            catch (Exception)
            {
                                
            }
        }

        public void LogProgressUpdate(string Trasa, string IdPedido)
        {
            if (!(System.IO.Directory.Exists(Application.StartupPath + @"\\Errors\\LogUpdateProgress\\")))
            {
                System.IO.Directory.CreateDirectory(Application.StartupPath + @"\\Errors\\LogUpdateProgress\\");
            }
            FileStream fs = new FileStream(Application.StartupPath + @"\\Errors\\LogUpdateProgress\\Log_" + IdPedido + ".txt", FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter s = new StreamWriter(fs);
            s.Close();
            fs.Close();
            FileStream fs1 = new FileStream(Application.StartupPath + @"\\Errors\\LogUpdateProgress\\Log_" + IdPedido + ".txt", FileMode.Append, FileAccess.Write);
            StreamWriter s1 = new StreamWriter(fs1);

            string[] partes = Trasa.Split(new char[] { '\n' });
            foreach (string item in partes)
            {
                //s1.WriteLine(Trasa);   
                s1.WriteLine(item);
            }
            s1.Close();
            fs1.Close();
        }

        #endregion

    }
}
