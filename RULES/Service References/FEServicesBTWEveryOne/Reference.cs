﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RULES.FEServicesBTWEveryOne {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Factura", Namespace="http://schemas.datacontract.org/2004/07/FEServicesBTW_EveryOne.ServiceBTW")]
    [System.SerializableAttribute()]
    public partial class Factura : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string ClienteFacturaElectronicaField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string LogTimbradoField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string MensajeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PathPdfField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string PathXmlDianField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string QRImageBase64Field;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string RespuestaServicioField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string StrInvoiceTypeField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string TracerField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string idField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string strXmlARInvoiceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string successField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string ClienteFacturaElectronica {
            get {
                return this.ClienteFacturaElectronicaField;
            }
            set {
                if ((object.ReferenceEquals(this.ClienteFacturaElectronicaField, value) != true)) {
                    this.ClienteFacturaElectronicaField = value;
                    this.RaisePropertyChanged("ClienteFacturaElectronica");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string LogTimbrado {
            get {
                return this.LogTimbradoField;
            }
            set {
                if ((object.ReferenceEquals(this.LogTimbradoField, value) != true)) {
                    this.LogTimbradoField = value;
                    this.RaisePropertyChanged("LogTimbrado");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Mensaje {
            get {
                return this.MensajeField;
            }
            set {
                if ((object.ReferenceEquals(this.MensajeField, value) != true)) {
                    this.MensajeField = value;
                    this.RaisePropertyChanged("Mensaje");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PathPdf {
            get {
                return this.PathPdfField;
            }
            set {
                if ((object.ReferenceEquals(this.PathPdfField, value) != true)) {
                    this.PathPdfField = value;
                    this.RaisePropertyChanged("PathPdf");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string PathXmlDian {
            get {
                return this.PathXmlDianField;
            }
            set {
                if ((object.ReferenceEquals(this.PathXmlDianField, value) != true)) {
                    this.PathXmlDianField = value;
                    this.RaisePropertyChanged("PathXmlDian");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string QRImageBase64 {
            get {
                return this.QRImageBase64Field;
            }
            set {
                if ((object.ReferenceEquals(this.QRImageBase64Field, value) != true)) {
                    this.QRImageBase64Field = value;
                    this.RaisePropertyChanged("QRImageBase64");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string RespuestaServicio {
            get {
                return this.RespuestaServicioField;
            }
            set {
                if ((object.ReferenceEquals(this.RespuestaServicioField, value) != true)) {
                    this.RespuestaServicioField = value;
                    this.RaisePropertyChanged("RespuestaServicio");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string StrInvoiceType {
            get {
                return this.StrInvoiceTypeField;
            }
            set {
                if ((object.ReferenceEquals(this.StrInvoiceTypeField, value) != true)) {
                    this.StrInvoiceTypeField = value;
                    this.RaisePropertyChanged("StrInvoiceType");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Tracer {
            get {
                return this.TracerField;
            }
            set {
                if ((object.ReferenceEquals(this.TracerField, value) != true)) {
                    this.TracerField = value;
                    this.RaisePropertyChanged("Tracer");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string id {
            get {
                return this.idField;
            }
            set {
                if ((object.ReferenceEquals(this.idField, value) != true)) {
                    this.idField = value;
                    this.RaisePropertyChanged("id");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string strXmlARInvoice {
            get {
                return this.strXmlARInvoiceField;
            }
            set {
                if ((object.ReferenceEquals(this.strXmlARInvoiceField, value) != true)) {
                    this.strXmlARInvoiceField = value;
                    this.RaisePropertyChanged("strXmlARInvoice");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string success {
            get {
                return this.successField;
            }
            set {
                if ((object.ReferenceEquals(this.successField, value) != true)) {
                    this.successField = value;
                    this.RaisePropertyChanged("success");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="FEServicesBTWEveryOne.IFEServicesBTW")]
    public interface IFEServicesBTW {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFEServicesBTW/RecepcionXmlFromERP", ReplyAction="http://tempuri.org/IFEServicesBTW/RecepcionXmlFromERPResponse")]
        RULES.FEServicesBTWEveryOne.Factura RecepcionXmlFromERP(string prmInvoiceType, string prmXmlARInvoice);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFEServicesBTW/GetPathpFilesFE", ReplyAction="http://tempuri.org/IFEServicesBTW/GetPathpFilesFEResponse")]
        RULES.FEServicesBTWEveryOne.Factura GetPathpFilesFE(string prmComapnyID, string prmNit, string prmInvoiceNum, string prmLegalNumber);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFEServicesBTW/GetImgQRInvoice", ReplyAction="http://tempuri.org/IFEServicesBTW/GetImgQRInvoiceResponse")]
        RULES.FEServicesBTWEveryOne.Factura GetImgQRInvoice(string prmXmlARInvoice);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IFEServicesBTW/ResendEmailInvoice", ReplyAction="http://tempuri.org/IFEServicesBTW/ResendEmailInvoiceResponse")]
        RULES.FEServicesBTWEveryOne.Factura ResendEmailInvoice(string prmComapnyID, string prmNit, string prmInvoiceNum, string prmLegalNumber, string EmailDestino);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IFEServicesBTWChannel : RULES.FEServicesBTWEveryOne.IFEServicesBTW, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class FEServicesBTWClient : System.ServiceModel.ClientBase<RULES.FEServicesBTWEveryOne.IFEServicesBTW>, RULES.FEServicesBTWEveryOne.IFEServicesBTW {
        
        public FEServicesBTWClient() {
        }
        
        public FEServicesBTWClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public FEServicesBTWClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FEServicesBTWClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public FEServicesBTWClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public RULES.FEServicesBTWEveryOne.Factura RecepcionXmlFromERP(string prmInvoiceType, string prmXmlARInvoice) {
            return base.Channel.RecepcionXmlFromERP(prmInvoiceType, prmXmlARInvoice);
        }
        
        public RULES.FEServicesBTWEveryOne.Factura GetPathpFilesFE(string prmComapnyID, string prmNit, string prmInvoiceNum, string prmLegalNumber) {
            return base.Channel.GetPathpFilesFE(prmComapnyID, prmNit, prmInvoiceNum, prmLegalNumber);
        }
        
        public RULES.FEServicesBTWEveryOne.Factura GetImgQRInvoice(string prmXmlARInvoice) {
            return base.Channel.GetImgQRInvoice(prmXmlARInvoice);
        }
        
        public RULES.FEServicesBTWEveryOne.Factura ResendEmailInvoice(string prmComapnyID, string prmNit, string prmInvoiceNum, string prmLegalNumber, string EmailDestino) {
            return base.Channel.ResendEmailInvoice(prmComapnyID, prmNit, prmInvoiceNum, prmLegalNumber, EmailDestino);
        }
    }
}
