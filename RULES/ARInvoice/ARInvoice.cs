﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

//Referenciar y usar.
using System.IO;
using System.Windows.Forms;
using System.Data;
using System.Configuration;
using System.Net;
using System.Xml;


using System.Net;
using System.ServiceModel.Channels;

namespace RULES.ARInvoice
{
    public class ARInvoice
    {
        #region "Constructor"

        public ARInvoice()
        {             
             //objDsDatos=null;
             //objdsARInvoice=null;
             //objdsArInvoiceList=null;
             strCompany = string.Empty;
             intInvoiceNum=-1;
             strLegalNumPruebas = string.Empty;
             strEstadoTimbrado=string.Empty;
             strCustId=string.Empty;
             strFechaI=string.Empty;       
             strFechaF=string.Empty;
             strXmlARInvoice=string.Empty;             
             objdtCompany = null;
             strRutaExport = string.Empty;
             strUriWS = string.Empty;             
             strError=string.Empty;
        }

        #endregion

        #region "Atributos"
        
        private DataSet objDsDatos;        
        //private ARInvoiceDataSet objdsARInvoice;
        //private InvcHeadListDataSet objdsArInvoiceList;
        private string strCompany;
        private string strNIT;
        private string strInvoiceType;
        private int intInvoiceNum;
        private string strLegalNumPruebas;
        private string strEstadoTimbrado;
        private string strCustId;
        private string strCusNum;
        private string strFechaI;       
        private string strFechaF;
        private bool _ManualInvoice;
        private DataSet _DsManualInvoice;
        string strXmlARInvoice;        
        private DataTable objdtCompany;
        private DataTable objdtSalesTax;
        private string strRutaExport;
        private string strUriWS;
        private string _PathPdf;
        private string _PatXmlDian;
        private string _LogTimbrado;
        private string strError;

        #endregion

        #region "Propiedades"

        public DataSet DsDatos
        {
            set { objDsDatos = value; }
            get { return objDsDatos; }
        }    

        public string Company
        {
            set { strCompany = value; }
        }        

        public string NIT
        { set { strNIT = value; } }

        public int InvoiceNum
        {
            set { intInvoiceNum = value; }
        }

        public string LegalNumPruebas
        {
            set { strLegalNumPruebas = value; }
            get { return strLegalNumPruebas; }
        }

        public string InvoiceType
        {
            set { strInvoiceType = value; }            
        }

        public string EstadoTimbrado
        {
            set { strEstadoTimbrado = value; }
            get { return strEstadoTimbrado; }
        }

        public string CustId
        {
            set { strCustId = value; }
            get { return strCustId; }
        }

        public string CustNum
        {
            set { strCusNum = value; }
            get { return strCusNum; }
        }

        public string FechaI
        {
            get { return strFechaI; }
            set { strFechaI = value; }
        }

        public string FechaF
        {
            get { return strFechaF; }
            set { strFechaF = value; }
        }

        public bool ManualInvoice
        {
            set { _ManualInvoice = value; }
        }

        public DataSet DsManualInvoice
        {
            set { _DsManualInvoice = value; }
        }

        public string XmlARInvoice
        {
            set { strXmlARInvoice = value; }
            get { return strXmlARInvoice; }
        }
        
        public string RutaExport
        {
            set { strRutaExport = value; }
        }

        public string PathPdf
        {
            get {return _PathPdf; }
        }

        public string PathXmlDian
        {
            get { return _PatXmlDian; }
        }

        public string LogTimbrado
        {
            get { return _LogTimbrado; }
        }

        public string Error
        {
            get { return strError; }
        }         

        #endregion
        
        #region "Metodos privados"

        private string FechaSQL(string Fecha)
        {
            try
            {
                string NewDate = string.Format("{0:yyyyMMdd}", Convert.ToDateTime(Fecha));
                return NewDate;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return null;
            }

        }

        private string AddFiltros()
        {
            try
            {
                string were =string.Empty;
                if (!string.IsNullOrEmpty(strEstadoTimbrado) && strEstadoTimbrado != "Todas")
                {
                    if (string.IsNullOrEmpty(were))
                        were = " EstadoTimbre_c="+strEstadoTimbrado;
                    else
                        were += " and EstadoTimbre_c=" + strEstadoTimbrado;
                }

                if (!string.IsNullOrEmpty(strCustId))
                { 
                    if (string.IsNullOrEmpty(were))
                        were = " CustNum="+strCusNum;
                    else
                        were += " and CustNum=" + strCusNum;
                }

                if (!string.IsNullOrEmpty(strFechaI))
                {
                    if (string.IsNullOrEmpty(were))
                        were = " InvoiceDate >= '"+ FechaSQL(strFechaI)+"' ";
                    else
                        were += " and InvoiceDate >= '"+ FechaSQL(strFechaI)+"' ";
                }

                if (!string.IsNullOrEmpty(strFechaF))
                {
                    if (string.IsNullOrEmpty(were))
                        were = " InvoiceDate <= '"+ FechaSQL(strFechaF)+"'";
                    else
                        were += " and InvoiceDate <= '"+ FechaSQL(strFechaF)+"'";
                }

                if (string.IsNullOrEmpty(were))
                    were = " Posted =1 ";
                else
                    were += " and Posted =1";

                return were;

            }
            catch (Exception ex)
            {
                strError = "Error al aplicar filtros:"+ex.Message;
                return null;
            }
        }

        //private bool AddInfoCompany(string Company)
        //{
        //    //try
        //    //{
        //    //    if (objdtCompany == null)
        //    //    {
        //    //        CompanyBTW objCompany = new CompanyBTW();
        //    //        objCompany.oTrans = objTrans;

        //    //        if (!objCompany.GetByID(Company))
        //    //            throw new Exception("Error al buscar información de la compañia que factura en EPICOR: " + objCompany.Error);

        //    //        objdtCompany = objCompany.DsCompany.Tables["Company"].Copy();
        //    //        objdsARInvoice.Tables.Add(objdtCompany.Copy());
        //    //    }
        //    //    else
        //    //    {
        //    //        objdsARInvoice.Tables.Add(objdtCompany.Copy());
        //    //    }
                
        //    //    return true;
        //    //}
        //    //catch (Exception ex)
        //    //{
        //    //    strError = ex.Message;
        //    //    MessageBox.Show(Error); 
        //    //    return false;
        //    //}
        //}

        //private bool AddInfoCustomer(int CustNum)
        //{
        //    try
        //    {                
        //        CustomerBTW objCustomer = new CustomerBTW();
        //        objCustomer.oTrans = objTrans;

        //        if (!objCustomer.GetByCustID(CustNum))
        //            throw new Exception("Error al buscar información del cleinte en EPICOR: " + objCustomer.Error);

        //        objdsARInvoice.Tables.Add(objCustomer.DsCustomer.Tables["Customer"].Copy());
        //        //strXmlCustomer = objCustomer.DsCustomer.GetXml();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {                
        //        strError = ex.Message;
        //        return false;
        //    }
        //}

        //private bool AddInfoCoOneTime(string OneTimeID)
        //{
        //    try
        //    {                
        //        CoOneTimeBTW objCoOneTime = new CoOneTimeBTW();
        //        objCoOneTime.oTrans = objTrans;

        //        if (!objCoOneTime.GetByOneTimeID(OneTimeID))
        //            throw new Exception("Error al buscar información del OneTime en EPICOR: " + objCoOneTime.Error);                

        //        objdsARInvoice.Tables.Add(objCoOneTime.DsCoOneTime.Tables["COOneTime"].Copy());                
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        strError = ex.Message;
        //        return false;
        //    }
        //}

        //private bool AddInfoSalesTax(string Company)
        //{
        //    try
        //    {
        //        if (objdtSalesTax == null)
        //        {
        //            SalesTaxBTW  objSalesTax = new SalesTaxBTW();
        //            objSalesTax.oTrans = objTrans;

        //            if (!objSalesTax.GetRows(Company))
        //                throw new Exception(objSalesTax.Error);

        //            objdtSalesTax = objSalesTax.dsSalesTax.Tables["SalesTRC"].Copy();
        //            objdsARInvoice.Tables.Add(objdtSalesTax.Copy());
        //        }
        //        else
        //        {
        //            objdsARInvoice.Tables.Add(objdtSalesTax.Copy());                
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        strError = ex.Message;
        //        MessageBox.Show(Error);
        //        return false;
        //    }
        //}

        //private bool ExportarXMl(string XmlToExport, int intInvoiceNum)
        //{
        //    try
        //    {              
        //        if (strRutaExport != null && strRutaExport.Length > 0)                
        //            File.WriteAllText(strRutaExport +"\\XmlARInvoice_" + intInvoiceNum+".xml", XmlToExport);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        strError = "Error al exportar archivo: " + ex.Message;
        //        return false;                
        //    }
        //}

        private string CargarInformacionFacturaManualToTimber()
        {
            try
            {
                string RutaXmlStandar = AppDomain.CurrentDomain.BaseDirectory.ToString() + "xmlStandarBTW\\XmlStandarBTW.xml";
                XmlDocument XmlInvoiceTemplete = new XmlDocument();
                XmlInvoiceTemplete.Load(RutaXmlStandar);

                StringReader xmlSource = new StringReader(XmlInvoiceTemplete.InnerXml);

                DataSet DsInvoiceToSend = new DataSet();
                DsInvoiceToSend.ReadXml(xmlSource);

                return null;

                //1. Cargamos Xml de Company.                
                //ARInvoiceBTW objARInvoice = new ARInvoiceBTW();
                //objARInvoice.oTrans = objTrans;

                //if (!objARInvoice.GetByID(intInvoiceNum))
                //    throw new Exception("No se cargo la factura:" + objARInvoice.Error);

                //objdsARInvoice = objARInvoice.dsARInvoice;

                ////Numerao Legal Para Pruebas                
                //if (!string.IsNullOrEmpty(strLegalNumPruebas))
                //{
                //    objdsARInvoice.Tables["InvcHead"].Rows[0]["LegalNumber"] = strLegalNumPruebas;
                //}

                //if (!AddInfoCompany(strCompany))
                //    throw new Exception("Error al adicionar información de la compañia al Xml de la factura.");

                //if (!AddInfoSalesTax(strCompany))
                //    throw new Exception("Error al adicionar información de SalesTax al Xml de la factura.");

                //if (!AddInfoCustomer(Convert.ToInt32(objdsARInvoice.Tables["InvcHead"].Rows[0]["CustNum"])))
                //    throw new Exception("Erro al adicionar información del cliente al XML de factura");

                //if (!AddInfoCoOneTime(objdsARInvoice.Tables["Customer"].Rows[0]["ResaleID"].ToString()))
                //    throw new Exception("Erro al adicionar información del CoOneTime al XML de factura");

                //strXmlARInvoice = objdsARInvoice.GetXml();

                //Cargamos XML.
                string _RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice\\XmlARInvoiceExample.xml";
                XmlDocument _XmlInvoice = new XmlDocument();
                _XmlInvoice.Load(_RutaXmlInvoice);

                strXmlARInvoice = _XmlInvoice.InnerXml;
                return (strXmlARInvoice);
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return null;
            }
        }

        private string CargarInformacionFacturaToTimber()
        {
            try
            {
                string RutaXmlStandar = AppDomain.CurrentDomain.BaseDirectory.ToString() + "xmlStandarBTW\\XmlStandarBTW.xml";
                XmlDocument XmlInvoiceTemplete = new XmlDocument();
                XmlInvoiceTemplete.Load(RutaXmlStandar);

                StringReader xmlSource = new StringReader(XmlInvoiceTemplete.InnerXml);

                DataSet DsInvoiceToSend = new DataSet();
                DsInvoiceToSend.ReadXml(xmlSource);

                return null;

                //1. Cargamos Xml de Company.                
                //ARInvoiceBTW objARInvoice = new ARInvoiceBTW();
                //objARInvoice.oTrans = objTrans;

                //if (!objARInvoice.GetByID(intInvoiceNum))
                //    throw new Exception("No se cargo la factura:" + objARInvoice.Error);

                //objdsARInvoice = objARInvoice.dsARInvoice;

                ////Numerao Legal Para Pruebas                
                //if (!string.IsNullOrEmpty(strLegalNumPruebas))
                //{
                //    objdsARInvoice.Tables["InvcHead"].Rows[0]["LegalNumber"] = strLegalNumPruebas;
                //}

                //if (!AddInfoCompany(strCompany))
                //    throw new Exception("Error al adicionar información de la compañia al Xml de la factura.");

                //if (!AddInfoSalesTax(strCompany))
                //    throw new Exception("Error al adicionar información de SalesTax al Xml de la factura.");

                //if (!AddInfoCustomer(Convert.ToInt32(objdsARInvoice.Tables["InvcHead"].Rows[0]["CustNum"])))
                //    throw new Exception("Erro al adicionar información del cliente al XML de factura");

                //if (!AddInfoCoOneTime(objdsARInvoice.Tables["Customer"].Rows[0]["ResaleID"].ToString()))
                //    throw new Exception("Erro al adicionar información del CoOneTime al XML de factura");

                //strXmlARInvoice = objdsARInvoice.GetXml();

                //Cargamos XML.
                string _RutaXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice\\XmlARInvoiceExample.xml";
                XmlDocument _XmlInvoice = new XmlDocument();
                _XmlInvoice.Load(_RutaXmlInvoice);

                strXmlARInvoice = _XmlInvoice.InnerXml;
                return (strXmlARInvoice);
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return null;
            }
        }

        #endregion

        #region "Metodos Publicos" 

        public bool EnviarFacturaManualToBTW(ref string Tracer)
        {
            try
            {
                //1. Objeto Factura que almasenara el Response.
                FEServicesBTWEveryOne.Factura ResponseEnvioFE = new FEServicesBTWEveryOne.Factura();

                //2. Instancia del cliente.
                FEServicesBTWEveryOne.FEServicesBTWClient ws = new FEServicesBTWEveryOne.FEServicesBTWClient();                                

                //3. Adicionando Usuario y Password.
                ws.ClientCredentials.UserName.UserName = "**************";
                ws.ClientCredentials.UserName.Password = "**************";

                //4. Enviando informacion de autenticacion en el Header SOAP Request
                using (new OperationContextScope(ws.InnerChannel))
                {                    
                    string auth = string.Format("{0}", Convert.ToBase64String(System.Text.Encoding.Default.GetBytes(string.Format("{0}:{1}", ws.ClientCredentials.UserName.UserName, ws.ClientCredentials.UserName.Password))));
                    HttpRequestMessageProperty requestMessage = new HttpRequestMessageProperty();                    
                    requestMessage.Headers["Authorization"] = auth;
                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = requestMessage;
                    ResponseEnvioFE = ws.RecepcionXmlFromERP(strInvoiceType, strXmlARInvoice);
                }

                //5. Recibiento respuesta del servicio
                string AceptaDian = ResponseEnvioFE.success;//True - False.
                Tracer = ResponseEnvioFE.Tracer;


                return Convert.ToBoolean(AceptaDian);
            }
            catch (Exception ex)
            {
                return false;                
            }            
        }
        
        #endregion

    }
}
