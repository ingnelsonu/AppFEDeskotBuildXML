﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App_FEDeskot
{
    public partial class splashFEBtw : FormBase
    {
        #region "Constructor" 

        public splashFEBtw()
        {
            InitializeComponent();
        }

        #endregion 

        #region "Atributos"

        #endregion

        #region "Form Load"

        private void splashFEBtw_Load(object sender, EventArgs e)
        {            
            
        }

        private void splashFEBtw_Shown(object sender, EventArgs e)
        {
            
        }

        private void LoadSettings()
        {
            try
            {
                pgbCarga.Value = 10;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 20;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 30;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 40;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 50;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 60;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 70;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 80;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 90;
                for (int i = 0; i < 100000000; i++) ;

                pgbCarga.Value = 100;
                for (int i = 0; i < 100000000; i++) ;


                tmrLoad.Enabled = false;
                //MDIMaster MDI = new MDIMaster();
                //MDI.Show();
                System.Threading.Thread NuevoHilo = new System.Threading.Thread(new System.Threading.ThreadStart(InicarMaster));
                this.Close();
                NuevoHilo.SetApartmentState(System.Threading.ApartmentState.STA);
                NuevoHilo.Start();

            }
            catch (Exception ex)
            {
                MensajeErr(ex.Message);
            }
        }

        private void InicarMaster()
        {
            MDIMaster MDI = new MDIMaster();
            //MDI.Show();
            MDI.ShowDialog();
            this.Close();
        }

        private void tmrLoad_Tick(object sender, EventArgs e)
        {
            LoadSettings();
        }

        #endregion

        #region "Metodos Form"

        #endregion

        #region "Eventos controles"

        #endregion
    }
}
