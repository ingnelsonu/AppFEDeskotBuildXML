﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace App_FEDeskot
{
    public partial class PopupError : Form
    {
        #region "Atributos"

        private string _Titulo;
        private string _Mensaje;

        #endregion

        #region "Propiedades"

        public string Titulo
        {
            set { _Titulo = value; }
        }

        public string Mensaje
        {
            set { _Mensaje = value; }
        }

        #endregion

        #region "Form Load"

        public PopupError()
        {
            InitializeComponent();
        }

        private void PopupError_Load(object sender, EventArgs e)
        {
            this.lblTitulo.Text = _Titulo;
            this.txtMsj.Text = _Mensaje;
        }

        #endregion

    
    }
}
