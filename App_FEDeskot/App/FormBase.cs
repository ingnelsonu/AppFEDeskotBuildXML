﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

//Referenciar EPICOR.
using System.Xml;
using System.Data;


namespace App_FEDeskot
{
    public  partial class FormBase:Form
    {
        #region "Atributos"

        protected string _PathToSaveXmlInvoice;
        protected DataSet _DsSecurity;


        #endregion

        #region "Load"

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // FormBase
            // 
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Name = "FormBase";
            this.ResumeLayout(false);

        }

        #endregion

        #region "Mensajes Alertas"

        public void MensajeErr(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Factura Electronica - Err", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        public void MensajeWar(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Factura Electronica - War", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        public void MensajeOK(string Mensaje)
        {
            MessageBox.Show(Mensaje, "Factura Electronica - OK", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public bool MensajeQuestion(string Mensaje)
        {
            if (MessageBox.Show(Mensaje, "Factura Electronica - Question", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                return true;
            else
                return false; 
        }

        #endregion

        #region "Validadores"

        protected string FechaSQL(string Fecha)
        {
            try
            {
                string NewDate = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(Fecha));
                return NewDate;
            }
            catch (Exception)
            {
                return Fecha;
            }
        }

        protected bool ValidarNumericoSTR(string Valor, int LenMin = 0, int LenMax = 0)
        {
            //Valida cadenas de texto que solo contengan numeros del 0 al 9, cadena vacia retorna
            Valor = Valor.Replace(",", "").Replace(".", "");

            if (string.IsNullOrEmpty(Valor))
                return false;
            if (LenMin != 0)
                if (Valor.Length < LenMin)
                    return false;
            if (LenMax != 0)
                if (Valor.Length > LenMax)
                    return false;
            string CharVal = null;
            for (int i=1; i <=Valor.Length; i++)
            {
                CharVal = Mid(Valor, i-1, 1);
                if (!(System.Convert.ToChar(CharVal) > 47 && System.Convert.ToChar(CharVal) < 58))
                    return false;
                //48-57(0-9)  
            }            

            return true;
        }

        protected bool ValidarDecimal(ref string Valor)
        {
            Valor = Valor.Replace(",", "").Replace(".", "");

            if (Valor == null)
            {
                Valor = "0";
            }
            else
            {
                string CharVal = null;
                bool SWp = false;
                for (int i =1; i <= Valor.Length; i++)
                {
                    if (System.Convert.ToChar(Mid(Valor, i-1, 1)) == 46)
                    {
                        if (SWp | Valor.Length == i | i == 1)
                            return false;
                        SWp = true;
                    }
                    else
                    {
                        CharVal =Mid(Valor, i-1, 1);
                        if (System.Convert.ToChar(CharVal) < 48 || System.Convert.ToChar(CharVal) > 57)
                            return false;
                    }
                }
            }
            return true;
        }

        protected bool ValidarTime(string Time)
        {
            try
            {
                DateTime TimeOut;
                if (!DateTime.TryParse(Time, out TimeOut))
                    return false;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        //protected string Left(string param, int length)
        //{
        //    string result = param.Substring(0, length);
        //    return result;
        //}

        //protected string Right(string param, int length)
        //{

        //    int value = param.Length - length;
        //    string result = param.Substring(value, length);
        //    return result;
        //}

        protected string Mid(string param, int startIndex, int length)
        {
            if (param.Length == 1)
                startIndex = 0;

            string result = param.Substring(startIndex, length);
            return result;
        }

        protected string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        protected string ReplaceDecimal(string Valor)
        {
            int IndexComa = 0;
            int IndexPunto = 0;

            CultureInfo Cultura = CultureInfo.CurrentCulture;
            string SPDecimal = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;            

            try
            {
                IndexPunto = Valor.IndexOf(".");
                IndexComa = Valor.IndexOf(",");           

                //Separador de miles =. decimales=,
                if (IndexPunto < IndexComa && IndexPunto!=-1)
                {
                    Valor = Valor.Replace(".", "").Replace(",", SPDecimal);
                }
                //Separador de miles =, decimales=.
                else if( IndexComa<IndexPunto && IndexComa!=-1)
                {
                    Valor = Valor.Replace(",","").Replace(".",SPDecimal);
                }
                //Separador decimal=,
                else if(IndexPunto==-1)
                    Valor = Valor.Replace(",", SPDecimal);
                //Separador decimal=.
                else if (IndexComa == -1)
                    Valor = Valor.Replace(".", SPDecimal);

                return Valor;

            }
            catch (Exception)
            {
                return Valor;
            }
        }

        protected bool ValidarMenu(Form MenuForm,DataSet DsSecurity,ref string Rpta)
        {
            try
            {
                string MenuName="";

                if (DsSecurity == null)
                    return false;

                if (MenuForm.Name.ToString().Contains(":"))
                    MenuName = MenuForm.Name.Substring(0, MenuForm.Name.IndexOf(":"));
                else
                    MenuName = MenuForm.Name;

                DataRow[] Modulo = DsSecurity.Tables["Modulos"].Select("IdModulo='" + MenuName + "'");
                if (Modulo != null && Modulo.GetLength(0) > 0)
                {
                    HideControlesForm(Convert.ToInt32(Modulo[0]["IdSeguridad"]), MenuForm, DsSecurity.Tables["Controles"]);
                    return true;
                }                    
                else
                {
                    Rpta = "No tienes permisos para acceder al Menu: " + MenuForm.Text + " contacta a sistemas para verificar tu configuración de seguridad.";
                    return false;
                }
            }
            catch (Exception ex)
            {
                Rpta = "Error al validar Menu: " + MenuForm.Text + " Err:" + ex.Message;
                return false;
            }
        }

        private void HideControlesForm(int IdSecurity,Form frm,DataTable DtControles)
        {
            try
            {
                DataRow[] Controles = DtControles.Select("IdSeguridad='" +IdSecurity+ "'");

                if (Controles!=null && Controles.GetLength(0)>0)
                {

                    foreach (Control ctrl in frm.Controls)
                    {
                        //Controles  Padre.
                        DataRow[] Control = DtControles.Select("IdControl='" + ctrl.Name + "'");

                        if (Control != null && Control.GetLength(0) > 0)
                        {
                            ctrl.Visible = Convert.ToBoolean(Control[0]["Visualizar"]);
                        }
                        //Controles Hijos
                        else 
                        {
                            if (ctrl.Controls.Count > 0)
                            {
                                foreach (Control ctrlChild in ctrl.Controls)
                                {
                                    DataRow[] ControlChild = DtControles.Select("IdControl='" + ctrlChild.Name + "'");

                                    if (ControlChild != null && ControlChild.GetLength(0) > 0)
                                    {
                                        ctrlChild.Visible = Convert.ToBoolean(ControlChild[0]["Visualizar"]);
                                    }
                                }
                            }
                        }

                        
                       
                    }
                 }
            }
            catch 
            {
                                
            }
        }

       

        #endregion

        #region "APP Config"

        public bool ConfigApp(string Proyecto, ref string[] Args)
        {
            try
            {
                string Path = Application.StartupPath;
                string AppName = Application.ProductName;
                int Index = Path.LastIndexOf("bin");
                string PathRaiz = Path.Substring(0, (Index - (AppName.Length + 1)));

                XmlDocument ConfigData = new XmlDocument();
                ConfigData.Load(PathRaiz +Proyecto+"\\App.config");
                XmlNodeList Config = ConfigData.GetElementsByTagName("configuration");

                //Nodo configuraciones de Usuarios
                XmlNodeList lista =
                    ((XmlElement)Config[0]).GetElementsByTagName("userSettings");


                //Nodo Propiedades
                XmlNodeList Setting =
                    ((XmlElement)lista[0]).GetElementsByTagName("DATA.Properties.Settings");

                //Nodos Values dendotro de DATA.Properties.Settings
                foreach (XmlElement nodo in Setting)
                {                

                    XmlNodeList nValue =
                    nodo.GetElementsByTagName("value");
                    Args = new string[nValue.Count];

                    for (int i = 0; i < nValue.Count; i++)
                    {
                        Args[i] = nValue[i].InnerText;
                    }

                    //MessageBox.Show("Value1=" + nValue[0].InnerText + "\nValue2=" + nValue[1].InnerText + "\nValue3=" + nValue[2].InnerText);
                }


                return true;

            }
            catch (Exception)
            {
                MensajeErr("Error al consultar configuracion del proyecto:" + Proyecto);
                return false;
            }
        }

        public void SetPathToSaveXmlInvoice()
        {
            try
            {
                if (App_FEDeskot.Properties.Settings.Default["PathToSaveXmlInvoice"].ToString() == "localhost")
                    _PathToSaveXmlInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice";                    
                else
                    _PathToSaveXmlInvoice = App_FEDeskot.Properties.Settings.Default["PathToSaveXmlInvoice"].ToString();
            }
            catch (Exception ex)
            {
                MensajeErr(ex.Message);
            }
        }

        #endregion

        #region "XML"

        public bool XML_SQL_ENCODING(DataTable dt, ref string xml)
        {
            try
            {
                xml = "<Lista>";
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    xml += "<Prm>";
                    for (int j = 0; j <= dt.Columns.Count - 1; j++)
                    {
                        xml += "<" + dt.Columns[j].ColumnName + ">";
                        xml += dt.Rows[i][j];
                        xml += "</" + dt.Columns[j].ColumnName + ">";
                    }
                    xml += "</Prm>";
                }
                xml += "</Lista>";
                return true;
            }
            catch 
            {
                return false;
            }
        }

        #endregion
    }
}
