﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

//Referenciar y usar.
using App_FEDeskot.MN.Invoice;

namespace App_FEDeskot
{
    public partial class MDIMaster : Form
    {
        private int childFormNumber = 0;

        public MDIMaster()
        {
            InitializeComponent();
        }

        private void MDIMaster_Load(object sender, EventArgs e)
        {
            try
            {
                //MessageBox.Show("Entrando a FE");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

        private void tvMain_AfterSelect(object sender, TreeViewEventArgs e)
        {
            switch (tvMain.SelectedNode.Name)
            {
                case "ConexionDB":
                    {
                        //DeleteObjects();
                        //ConfigcnxString frmcnx = new ConfigcnxString();
                        //frmcnx.MdiParent = this;
                        //pnlMain.Controls.Add(frmcnx);
                        //frmcnx.BringToFront();
                        //frmcnx.Show();
                        break;
                    }
                case "ManualInvoice":
                    {
                        DeleteObjects();
                        ManualInvoice frmcnx = new ManualInvoice();
                        frmcnx.MdiParent = this;
                        pnlMain.Controls.Add(frmcnx);
                        frmcnx.BringToFront();
                        frmcnx.Show();
                        break;
                    }
                case "ConsultarInvoice":
                    {
                        //DeleteObjects();
                        ////ConsultaFacturas frmcnx = new ConsultaFacturas();
                        //ConsultarFacturasBTW frmcnx = new ConsultarFacturasBTW();
                        //frmcnx.MdiParent = this;
                        //pnlMain.Controls.Add(frmcnx);
                        //frmcnx.BringToFront();
                        //frmcnx.Show();
                        break;
                    }
                case "NuevoCliente":
                    {
                        //DeleteObjects();
                        //Cliente frmcnx = new Cliente();
                        //frmcnx.MdiParent = this;
                        //pnlMain.Controls.Add(frmcnx);
                        //frmcnx.BringToFront();
                        //frmcnx.Show();
                        break;
                    }
                case "ConsultarCliente":
                    {
                        //DeleteObjects();
                        //Consultar frmcnx = new Consultar();
                        //frmcnx.MdiParent = this;
                        //pnlMain.Controls.Add(frmcnx);
                        //frmcnx.BringToFront();
                        //frmcnx.Show();
                        break;
                    }
            }

        }

        private void DeleteObjects()
        {
            try
            {
                foreach (Control ctrl in pnlMain.Controls)
                    ctrl.Dispose();
            }
            catch (Exception)
            {

            }
        }
    }
}
