﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Referenciar y usar.
using System.Xml;


namespace App_FEDeskot.MN.Invoice
{
    public partial class InvoiceLine : UserControl
    {
        #region "Constructor"

        public InvoiceLine()
        {
            InitializeComponent();
        }

        #endregion

        #region "Atributos"  

        //public delegate void TotalInvoice();
        //public event TotalInvoice SendTotalInvoice;

        public delegate void TotalInvoice();
        public event TotalInvoice SendTotalInvoice;
        private DataTable _DtSalesTRC;
        private decimal _UnitPrice=0;
        private decimal _Cantidad=0;
        private decimal _DspDocExtPrice=0;
        private string _RateCode=string.Empty;
        private decimal _Percent=0;        
        private decimal _TaxAmt =0;
        private decimal _DiscountPercent = 0;
        private decimal _DspDocLessDiscount=0;//Valor Descuento;
        private decimal _DspDocTotalMiscChrg = 0;//Cargos Miscellaneous
        private string _Error;

        #endregion

        #region "Propiedades"

        public int Line
        {
            set { lblInvoiceLine.Text = value.ToString(); }
            get { return Convert.ToInt32(lblInvoiceLine.Text); }
        }

        public string PartDescription
        {
            get { return txtPartNumPartDescription.Text; }
        }

        public decimal SellingShipQty
        {
            get { return Convert.ToDecimal(txtCantidad.Text == "" ? "0" : txtCantidad.Text); }
        }

        public decimal UnitPrice
        {
            get { return _UnitPrice; }
        }        

        public decimal DspDocExtPrice
        {
            get { return _DspDocExtPrice;}

        }

        public decimal DiscountPercent
        {
            get { return _DiscountPercent; }
        }

        public decimal DspDocLessDiscount
        {
            get { return _DspDocLessDiscount; }
        }

        public decimal TaxAmt
        {
            get { return _TaxAmt; }
        }

        public decimal Percent
        {
            get {return  _Percent;}
        }

        public string RateCode
        {
            get { return cmbImpuesto.SelectedValue.ToString(); }
        }

        public DataTable DtSalesTRC
        {
            get { return _DtSalesTRC; }
        }

        #endregion

        #region "Form Load"

        private void InvoiceLine_Load(object sender, EventArgs e)
        {
            try
            {
                if (!CargarImpuestos())
                    throw new Exception(_Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(""+ex.Message,"Invoice Line",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        #endregion

        #region "Metodos Form"

        protected bool ValidarDecimal(ref string Valor)
        {
            Valor = Valor.Replace(",", "").Replace(".", "");

            if (Valor == null)
            {
                Valor = "0";
            }
            else
            {
                string CharVal = null;
                bool SWp = false;
                for (int i = 1; i <= Valor.Length; i++)
                {
                    if (System.Convert.ToChar(Mid(Valor, i - 1, 1)) == 46)
                    {
                        if (SWp | Valor.Length == i | i == 1)
                            return false;
                        SWp = true;
                    }
                    else
                    {
                        CharVal = Mid(Valor, i - 1, 1);
                        if (System.Convert.ToChar(CharVal) < 48 || System.Convert.ToChar(CharVal) > 57)
                            return false;
                    }
                }
            }
            return true;
        }

        protected string Mid(string param, int startIndex, int length)
        {
            if (param.Length == 1)
                startIndex = 0;

            string result = param.Substring(startIndex, length);
            return result;
        }

        protected string Mid(string param, int startIndex)
        {
            string result = param.Substring(startIndex);
            return result;
        }

        private bool CargarImpuestos()
        {
            XmlDocument xmlSalesTRC = new XmlDocument();

            try
            {
                string RutaXmlSalesTRC = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\XmlSalesTRC.xml";
                xmlSalesTRC.Load(RutaXmlSalesTRC);

                //Cargamos Informacion por defecto.
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlSalesTRC.NameTable);
                string query = "SalesTRC/SaleTRC";

                XmlNodeList NodoSalesTRC = xmlSalesTRC.SelectNodes(query, manager);

                _DtSalesTRC = new DataTable();
                _DtSalesTRC.Columns.Add("Id", typeof(string));
                _DtSalesTRC.Columns.Add("TaxCode", typeof(string));
                _DtSalesTRC.Columns.Add("RateCode", typeof(string));
                _DtSalesTRC.Columns.Add("Name", typeof(string));
                _DtSalesTRC.Columns.Add("Percent", typeof(decimal));
                _DtSalesTRC.Columns.Add("IdImpDIAN_c", typeof(string));                

                foreach (XmlNode NodoTax in NodoSalesTRC)
                {
                    DataRow rowTax = _DtSalesTRC.NewRow();
                    rowTax["Id"] = NodoTax.ChildNodes[2].InnerText;
                    rowTax["Name"] = NodoTax.ChildNodes[3].InnerText;
                    rowTax["TaxCode"] = NodoTax.ChildNodes[1].InnerText;
                    rowTax["RateCode"] = NodoTax.ChildNodes[2].InnerText;
                    rowTax["Percent"] = NodoTax.ChildNodes[4].InnerText;
                    rowTax["IdImpDIAN_c"] = NodoTax.ChildNodes[5].InnerText;
                    _DtSalesTRC.Rows.Add(rowTax);
                }

                cmbImpuesto.DataSource = _DtSalesTRC;
                cmbImpuesto.ValueMember = "Id";
                cmbImpuesto.DisplayMember = "Name";
                cmbImpuesto.Refresh();

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Listado de impuestos: "+ex.Message;
                return false;
            }
            finally
            {
                xmlSalesTRC = null;
            }

        }

        private decimal CalExtPrice()
        {
            try
            {
                _DspDocExtPrice= (_UnitPrice * _Cantidad);
                return _DspDocExtPrice;
            }
            catch (Exception ex) 
            {               
               return 0;             
            }

        }

        private decimal CalImpuesto()
        {
            try
            {
                if (txtDspDocExtPrice.Text == string.Empty)
                    return 0;

                if (_Percent == 0)
                    _TaxAmt = 0;
                else
                    _TaxAmt = ((_DspDocExtPrice * _Percent) / 100);
                
                return _TaxAmt;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        private decimal CalDescuento()
        {          
            try
            {
                //Descuento por porcentaje.
                if(chkDescPercent.Checked)
                {
                    if (string.IsNullOrEmpty(txtPercentDesc.Text))
                        _DspDocLessDiscount = 0;

                    _DspDocLessDiscount = (_DspDocExtPrice * Convert.ToDecimal(txtPercentDesc.Text))/100;
                    txtDspDocLessDiscount.Text = _DspDocLessDiscount.ToString();
                }
                else
                {
                    if (string.IsNullOrEmpty(txtDspDocLessDiscount.Text))
                        _DspDocLessDiscount = 0;

                    //_DspDocLessDiscount = Convert.ToDecimal(txtDspDocLessDiscount.Text);
                }

                return _DspDocLessDiscount;
            }
            catch (Exception)
            {
                return 0;
            }

        }

        #endregion
        
        #region "Eventos Controles"

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            try
            {                
                string Val = txtUnitPrice.Text == string.Empty ? "0" : txtUnitPrice.Text;

                if (!ValidarDecimal(ref Val))
                {
                    MessageBox.Show("Ingresa un valor Unitario Valido.", "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtUnitPrice.Text = "0";
                    txtUnitPrice.Focus();
                    return;
                }

                _UnitPrice = Convert.ToDecimal(txtUnitPrice.Text == string.Empty ? "0" : txtUnitPrice.Text);
                txtDspDocExtPrice.Text = string.Format("{0:N4}", CalExtPrice());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al validar Precio Unitario: " + ex.Message,"Invoice Line",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        private void txtCantidad_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //if (string.IsNullOrEmpty(txtCantidad.Text))
                //    return;

                string Val = txtCantidad.Text == string.Empty ? "0" : txtCantidad.Text;

                if (!ValidarDecimal(ref Val))
                {
                    MessageBox.Show("Ingresa una cantidad Valida.", "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCantidad.Text = "0";
                    txtCantidad.Focus();
                    return;
                }

                _Cantidad = Convert.ToDecimal(txtCantidad.Text == string.Empty ? "0" : txtCantidad.Text);
                txtDspDocExtPrice.Text = string.Format("{0:N4}", CalExtPrice());

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al validar Cantidad: " + ex.Message, "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDspDocExtPrice_TextChanged(object sender, EventArgs e)
        {     
            txtTaxAmt.Text = string.Format("{0:N4}", CalImpuesto());
            SendTotalInvoice();
        }

        private void cmbImpuesto_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                _RateCode = cmbImpuesto.SelectedValue.ToString();

                DataRow[] Taxes = _DtSalesTRC.Select("Id='" + _RateCode + "'");

                if (Taxes != null && Taxes.GetLength(0) > 0)
                    _Percent = Convert.ToDecimal(Taxes[0]["Percent"]);

                txtTaxAmt.Text = string.Format("{0:N4}", CalImpuesto());
                SendTotalInvoice();
            }
            catch (Exception ex)
            {
                
            }
        }

        private void chkDescPercent_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDescPercent.Checked)
            {
                txtPercentDesc.ReadOnly = false;
                txtDspDocLessDiscount.ReadOnly = true;
            }
            else
            {
                txtPercentDesc.ReadOnly = true;
                txtDspDocLessDiscount.ReadOnly = false;
            }
        }

        private void txtPercentDesc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Val = txtPercentDesc.Text == string.Empty ? "0" : txtPercentDesc.Text;

                if (!ValidarDecimal(ref Val))
                {
                    MessageBox.Show("Ingresa un porcentaje de descuento valido.", "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPercentDesc.Text = "0";
                    _DiscountPercent = 0;
                    txtPercentDesc.Focus();
                    return;
                }

                _DiscountPercent = Convert.ToDecimal(txtPercentDesc.Text == string.Empty ? "0" : txtPercentDesc.Text);
                txtDspDocLessDiscount.Text = string.Format("{0:N4}", CalDescuento());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al validar valor de descuento: " + ex.Message, "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtDspDocLessDiscount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string Val = txtDspDocLessDiscount.Text == string.Empty ? "0" : txtDspDocLessDiscount.Text;

                if (!ValidarDecimal(ref Val))
                {
                    MessageBox.Show("Ingresa un valor de descuento valido.", "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDspDocLessDiscount.Text = "0";
                    txtDspDocLessDiscount.Focus();
                    return;
                }

                _DspDocLessDiscount = Convert.ToDecimal(txtDspDocLessDiscount.Text == string.Empty ? "0" : txtDspDocLessDiscount.Text);
                SendTotalInvoice();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al validar valor de descuento: " + ex.Message, "Invoice Line", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion        
     
    }
}
