﻿namespace App_FEDeskot.MN.Invoice
{
    partial class ManualInvoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbFcturador = new System.Windows.Forms.GroupBox();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.lblAddress1 = new System.Windows.Forms.Label();
            this.txtCity = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.lblState = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtNit = new System.Windows.Forms.TextBox();
            this.lblNit = new System.Windows.Forms.Label();
            this.txtCompanyID = new System.Windows.Forms.TextBox();
            this.lblCompanyID = new System.Windows.Forms.Label();
            this.grbCliente = new System.Windows.Forms.GroupBox();
            this.cmbPais = new System.Windows.Forms.ComboBox();
            this.lblPais = new System.Windows.Forms.Label();
            this.cmbCliente = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbCurrencyCode = new System.Windows.Forms.ComboBox();
            this.lblCurrencyCode = new System.Windows.Forms.Label();
            this.lblCellPhone = new System.Windows.Forms.Label();
            this.txtCellPhone = new System.Windows.Forms.TextBox();
            this.lblEmailClient = new System.Windows.Forms.Label();
            this.txtEmailClient = new System.Windows.Forms.TextBox();
            this.lblAddressClient = new System.Windows.Forms.Label();
            this.txtAddressClient = new System.Windows.Forms.TextBox();
            this.lblCityClient = new System.Windows.Forms.Label();
            this.txtCityClient = new System.Windows.Forms.TextBox();
            this.lblEstateClient = new System.Windows.Forms.Label();
            this.txtStateClient = new System.Windows.Forms.TextBox();
            this.lblNameCliente = new System.Windows.Forms.Label();
            this.txtNameCliente = new System.Windows.Forms.TextBox();
            this.cmbIdentificationType = new System.Windows.Forms.ComboBox();
            this.lblNitCliente = new System.Windows.Forms.Label();
            this.txtNitCliente = new System.Windows.Forms.TextBox();
            this.lblIdentificationType = new System.Windows.Forms.Label();
            this.grbInvcHead = new System.Windows.Forms.GroupBox();
            this.grbPrepayment = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.lblFechapp = new System.Windows.Forms.Label();
            this.txtVlrPrepaidment = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.txtInvoiceComment = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtOrderNum = new System.Windows.Forms.TextBox();
            this.lblOrderNum = new System.Windows.Forms.Label();
            this.txtInvoiceRef = new System.Windows.Forms.TextBox();
            this.lblInvoiceRef = new System.Windows.Forms.Label();
            this.cmbInvoiceType = new System.Windows.Forms.ComboBox();
            this.lblInvoiceType = new System.Windows.Forms.Label();
            this.dtpDueDate = new System.Windows.Forms.DateTimePicker();
            this.dtpInvoiceDate = new System.Windows.Forms.DateTimePicker();
            this.lblDueDate = new System.Windows.Forms.Label();
            this.lblInvoiceDate = new System.Windows.Forms.Label();
            this.txtLegalNumber = new System.Windows.Forms.TextBox();
            this.lblLegalNumber = new System.Windows.Forms.Label();
            this.txtInvoiceNum = new System.Windows.Forms.TextBox();
            this.lblInvoiceNum = new System.Windows.Forms.Label();
            this.btnTracer = new System.Windows.Forms.Button();
            this.grbInvDtl = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.pnlLines = new System.Windows.Forms.Panel();
            this.btnAddline = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDspDocSubTotal = new System.Windows.Forms.Label();
            this.lblDocTaxAmt = new System.Windows.Forms.Label();
            this.lblDspDocInvoiceAmt = new System.Windows.Forms.Label();
            this.txtDspDocSubTotal = new System.Windows.Forms.TextBox();
            this.txtDocTaxAmt = new System.Windows.Forms.TextBox();
            this.txtDspDocInvoiceAmt = new System.Windows.Forms.TextBox();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.pgbEnvio = new System.Windows.Forms.ProgressBar();
            this.grbFcturador.SuspendLayout();
            this.grbCliente.SuspendLayout();
            this.grbInvcHead.SuspendLayout();
            this.grbPrepayment.SuspendLayout();
            this.grbInvDtl.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbFcturador
            // 
            this.grbFcturador.Controls.Add(this.txtAddress1);
            this.grbFcturador.Controls.Add(this.lblAddress1);
            this.grbFcturador.Controls.Add(this.txtCity);
            this.grbFcturador.Controls.Add(this.lblCity);
            this.grbFcturador.Controls.Add(this.txtEstado);
            this.grbFcturador.Controls.Add(this.lblState);
            this.grbFcturador.Controls.Add(this.txtNombre);
            this.grbFcturador.Controls.Add(this.lblName);
            this.grbFcturador.Controls.Add(this.txtNit);
            this.grbFcturador.Controls.Add(this.lblNit);
            this.grbFcturador.Controls.Add(this.txtCompanyID);
            this.grbFcturador.Controls.Add(this.lblCompanyID);
            this.grbFcturador.Location = new System.Drawing.Point(12, 13);
            this.grbFcturador.Name = "grbFcturador";
            this.grbFcturador.Size = new System.Drawing.Size(494, 213);
            this.grbFcturador.TabIndex = 0;
            this.grbFcturador.TabStop = false;
            this.grbFcturador.Text = "Facturador Electrónico (Bythewave)";
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(131, 151);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.ReadOnly = true;
            this.txtAddress1.Size = new System.Drawing.Size(240, 20);
            this.txtAddress1.TabIndex = 11;
            // 
            // lblAddress1
            // 
            this.lblAddress1.AutoSize = true;
            this.lblAddress1.Location = new System.Drawing.Point(6, 154);
            this.lblAddress1.Name = "lblAddress1";
            this.lblAddress1.Size = new System.Drawing.Size(55, 13);
            this.lblAddress1.TabIndex = 10;
            this.lblAddress1.Text = "Dirección:";
            // 
            // txtCity
            // 
            this.txtCity.Location = new System.Drawing.Point(131, 125);
            this.txtCity.Name = "txtCity";
            this.txtCity.ReadOnly = true;
            this.txtCity.Size = new System.Drawing.Size(240, 20);
            this.txtCity.TabIndex = 9;
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(6, 128);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(43, 13);
            this.lblCity.TabIndex = 8;
            this.lblCity.Text = "Ciudad:";
            // 
            // txtEstado
            // 
            this.txtEstado.Location = new System.Drawing.Point(131, 99);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.ReadOnly = true;
            this.txtEstado.Size = new System.Drawing.Size(240, 20);
            this.txtEstado.TabIndex = 7;
            // 
            // lblState
            // 
            this.lblState.AutoSize = true;
            this.lblState.Location = new System.Drawing.Point(6, 102);
            this.lblState.Name = "lblState";
            this.lblState.Size = new System.Drawing.Size(43, 13);
            this.lblState.TabIndex = 6;
            this.lblState.Text = "Estado:";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(131, 73);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.ReadOnly = true;
            this.txtNombre.Size = new System.Drawing.Size(240, 20);
            this.txtNombre.TabIndex = 5;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 76);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(47, 13);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "Nombre:";
            // 
            // txtNit
            // 
            this.txtNit.Location = new System.Drawing.Point(131, 47);
            this.txtNit.Name = "txtNit";
            this.txtNit.ReadOnly = true;
            this.txtNit.Size = new System.Drawing.Size(179, 20);
            this.txtNit.TabIndex = 3;
            // 
            // lblNit
            // 
            this.lblNit.AutoSize = true;
            this.lblNit.Location = new System.Drawing.Point(6, 50);
            this.lblNit.Name = "lblNit";
            this.lblNit.Size = new System.Drawing.Size(28, 13);
            this.lblNit.TabIndex = 2;
            this.lblNit.Text = "NIT:";
            // 
            // txtCompanyID
            // 
            this.txtCompanyID.Location = new System.Drawing.Point(131, 21);
            this.txtCompanyID.Name = "txtCompanyID";
            this.txtCompanyID.ReadOnly = true;
            this.txtCompanyID.Size = new System.Drawing.Size(179, 20);
            this.txtCompanyID.TabIndex = 1;
            // 
            // lblCompanyID
            // 
            this.lblCompanyID.AutoSize = true;
            this.lblCompanyID.Location = new System.Drawing.Point(6, 24);
            this.lblCompanyID.Name = "lblCompanyID";
            this.lblCompanyID.Size = new System.Drawing.Size(65, 13);
            this.lblCompanyID.TabIndex = 0;
            this.lblCompanyID.Text = "CompanyID:";
            // 
            // grbCliente
            // 
            this.grbCliente.Controls.Add(this.cmbPais);
            this.grbCliente.Controls.Add(this.lblPais);
            this.grbCliente.Controls.Add(this.cmbCliente);
            this.grbCliente.Controls.Add(this.label10);
            this.grbCliente.Controls.Add(this.cmbCurrencyCode);
            this.grbCliente.Controls.Add(this.lblCurrencyCode);
            this.grbCliente.Controls.Add(this.lblCellPhone);
            this.grbCliente.Controls.Add(this.txtCellPhone);
            this.grbCliente.Controls.Add(this.lblEmailClient);
            this.grbCliente.Controls.Add(this.txtEmailClient);
            this.grbCliente.Controls.Add(this.lblAddressClient);
            this.grbCliente.Controls.Add(this.txtAddressClient);
            this.grbCliente.Controls.Add(this.lblCityClient);
            this.grbCliente.Controls.Add(this.txtCityClient);
            this.grbCliente.Controls.Add(this.lblEstateClient);
            this.grbCliente.Controls.Add(this.txtStateClient);
            this.grbCliente.Controls.Add(this.lblNameCliente);
            this.grbCliente.Controls.Add(this.txtNameCliente);
            this.grbCliente.Controls.Add(this.cmbIdentificationType);
            this.grbCliente.Controls.Add(this.lblNitCliente);
            this.grbCliente.Controls.Add(this.txtNitCliente);
            this.grbCliente.Controls.Add(this.lblIdentificationType);
            this.grbCliente.Location = new System.Drawing.Point(522, 13);
            this.grbCliente.Name = "grbCliente";
            this.grbCliente.Size = new System.Drawing.Size(504, 213);
            this.grbCliente.TabIndex = 1;
            this.grbCliente.TabStop = false;
            this.grbCliente.Text = "Cliente (Adquiriente)";
            // 
            // cmbPais
            // 
            this.cmbPais.Enabled = false;
            this.cmbPais.FormattingEnabled = true;
            this.cmbPais.Location = new System.Drawing.Point(106, 117);
            this.cmbPais.Name = "cmbPais";
            this.cmbPais.Size = new System.Drawing.Size(192, 21);
            this.cmbPais.TabIndex = 22;
            // 
            // lblPais
            // 
            this.lblPais.AutoSize = true;
            this.lblPais.Location = new System.Drawing.Point(4, 117);
            this.lblPais.Name = "lblPais";
            this.lblPais.Size = new System.Drawing.Size(30, 13);
            this.lblPais.TabIndex = 21;
            this.lblPais.Text = "Pais:";
            // 
            // cmbCliente
            // 
            this.cmbCliente.FormattingEnabled = true;
            this.cmbCliente.Location = new System.Drawing.Point(106, 20);
            this.cmbCliente.Name = "cmbCliente";
            this.cmbCliente.Size = new System.Drawing.Size(192, 21);
            this.cmbCliente.TabIndex = 20;
            this.cmbCliente.SelectedIndexChanged += new System.EventHandler(this.cmbCliente_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(98, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Seleccione Cliente:";
            // 
            // cmbCurrencyCode
            // 
            this.cmbCurrencyCode.FormattingEnabled = true;
            this.cmbCurrencyCode.Location = new System.Drawing.Point(363, 73);
            this.cmbCurrencyCode.Name = "cmbCurrencyCode";
            this.cmbCurrencyCode.Size = new System.Drawing.Size(135, 21);
            this.cmbCurrencyCode.TabIndex = 18;
            // 
            // lblCurrencyCode
            // 
            this.lblCurrencyCode.AutoSize = true;
            this.lblCurrencyCode.Location = new System.Drawing.Point(309, 76);
            this.lblCurrencyCode.Name = "lblCurrencyCode";
            this.lblCurrencyCode.Size = new System.Drawing.Size(49, 13);
            this.lblCurrencyCode.TabIndex = 17;
            this.lblCurrencyCode.Text = "Moneda:";
            // 
            // lblCellPhone
            // 
            this.lblCellPhone.AutoSize = true;
            this.lblCellPhone.Location = new System.Drawing.Point(315, 51);
            this.lblCellPhone.Name = "lblCellPhone";
            this.lblCellPhone.Size = new System.Drawing.Size(42, 13);
            this.lblCellPhone.TabIndex = 16;
            this.lblCellPhone.Text = "Celular:";
            // 
            // txtCellPhone
            // 
            this.txtCellPhone.Location = new System.Drawing.Point(363, 48);
            this.txtCellPhone.Name = "txtCellPhone";
            this.txtCellPhone.ReadOnly = true;
            this.txtCellPhone.Size = new System.Drawing.Size(135, 20);
            this.txtCellPhone.TabIndex = 15;
            // 
            // lblEmailClient
            // 
            this.lblEmailClient.AutoSize = true;
            this.lblEmailClient.Location = new System.Drawing.Point(318, 26);
            this.lblEmailClient.Name = "lblEmailClient";
            this.lblEmailClient.Size = new System.Drawing.Size(35, 13);
            this.lblEmailClient.TabIndex = 14;
            this.lblEmailClient.Text = "Email:";
            // 
            // txtEmailClient
            // 
            this.txtEmailClient.Location = new System.Drawing.Point(363, 21);
            this.txtEmailClient.Name = "txtEmailClient";
            this.txtEmailClient.ReadOnly = true;
            this.txtEmailClient.Size = new System.Drawing.Size(135, 20);
            this.txtEmailClient.TabIndex = 13;
            // 
            // lblAddressClient
            // 
            this.lblAddressClient.AutoSize = true;
            this.lblAddressClient.Location = new System.Drawing.Point(4, 193);
            this.lblAddressClient.Name = "lblAddressClient";
            this.lblAddressClient.Size = new System.Drawing.Size(55, 13);
            this.lblAddressClient.TabIndex = 12;
            this.lblAddressClient.Text = "Dirección:";
            // 
            // txtAddressClient
            // 
            this.txtAddressClient.Location = new System.Drawing.Point(106, 189);
            this.txtAddressClient.Name = "txtAddressClient";
            this.txtAddressClient.ReadOnly = true;
            this.txtAddressClient.Size = new System.Drawing.Size(192, 20);
            this.txtAddressClient.TabIndex = 11;
            // 
            // lblCityClient
            // 
            this.lblCityClient.AutoSize = true;
            this.lblCityClient.Location = new System.Drawing.Point(4, 170);
            this.lblCityClient.Name = "lblCityClient";
            this.lblCityClient.Size = new System.Drawing.Size(43, 13);
            this.lblCityClient.TabIndex = 10;
            this.lblCityClient.Text = "Ciudad:";
            // 
            // txtCityClient
            // 
            this.txtCityClient.Location = new System.Drawing.Point(106, 166);
            this.txtCityClient.Name = "txtCityClient";
            this.txtCityClient.ReadOnly = true;
            this.txtCityClient.Size = new System.Drawing.Size(192, 20);
            this.txtCityClient.TabIndex = 9;
            // 
            // lblEstateClient
            // 
            this.lblEstateClient.AutoSize = true;
            this.lblEstateClient.Location = new System.Drawing.Point(4, 147);
            this.lblEstateClient.Name = "lblEstateClient";
            this.lblEstateClient.Size = new System.Drawing.Size(43, 13);
            this.lblEstateClient.TabIndex = 8;
            this.lblEstateClient.Text = "Estado:";
            // 
            // txtStateClient
            // 
            this.txtStateClient.Location = new System.Drawing.Point(106, 143);
            this.txtStateClient.Name = "txtStateClient";
            this.txtStateClient.ReadOnly = true;
            this.txtStateClient.Size = new System.Drawing.Size(192, 20);
            this.txtStateClient.TabIndex = 7;
            // 
            // lblNameCliente
            // 
            this.lblNameCliente.AutoSize = true;
            this.lblNameCliente.Location = new System.Drawing.Point(4, 96);
            this.lblNameCliente.Name = "lblNameCliente";
            this.lblNameCliente.Size = new System.Drawing.Size(47, 13);
            this.lblNameCliente.TabIndex = 6;
            this.lblNameCliente.Text = "Nombre:";
            // 
            // txtNameCliente
            // 
            this.txtNameCliente.Location = new System.Drawing.Point(106, 92);
            this.txtNameCliente.Name = "txtNameCliente";
            this.txtNameCliente.ReadOnly = true;
            this.txtNameCliente.Size = new System.Drawing.Size(192, 20);
            this.txtNameCliente.TabIndex = 5;
            // 
            // cmbIdentificationType
            // 
            this.cmbIdentificationType.Enabled = false;
            this.cmbIdentificationType.FormattingEnabled = true;
            this.cmbIdentificationType.Location = new System.Drawing.Point(106, 46);
            this.cmbIdentificationType.Name = "cmbIdentificationType";
            this.cmbIdentificationType.Size = new System.Drawing.Size(192, 21);
            this.cmbIdentificationType.TabIndex = 4;
            // 
            // lblNitCliente
            // 
            this.lblNitCliente.AutoSize = true;
            this.lblNitCliente.Location = new System.Drawing.Point(4, 73);
            this.lblNitCliente.Name = "lblNitCliente";
            this.lblNitCliente.Size = new System.Drawing.Size(28, 13);
            this.lblNitCliente.TabIndex = 3;
            this.lblNitCliente.Text = "NIT:";
            // 
            // txtNitCliente
            // 
            this.txtNitCliente.Location = new System.Drawing.Point(106, 69);
            this.txtNitCliente.Name = "txtNitCliente";
            this.txtNitCliente.ReadOnly = true;
            this.txtNitCliente.Size = new System.Drawing.Size(192, 20);
            this.txtNitCliente.TabIndex = 2;
            // 
            // lblIdentificationType
            // 
            this.lblIdentificationType.AutoSize = true;
            this.lblIdentificationType.Location = new System.Drawing.Point(4, 46);
            this.lblIdentificationType.Name = "lblIdentificationType";
            this.lblIdentificationType.Size = new System.Drawing.Size(97, 13);
            this.lblIdentificationType.TabIndex = 1;
            this.lblIdentificationType.Text = "Tipo Identificación:";
            // 
            // grbInvcHead
            // 
            this.grbInvcHead.Controls.Add(this.grbPrepayment);
            this.grbInvcHead.Controls.Add(this.txtInvoiceComment);
            this.grbInvcHead.Controls.Add(this.label9);
            this.grbInvcHead.Controls.Add(this.txtOrderNum);
            this.grbInvcHead.Controls.Add(this.lblOrderNum);
            this.grbInvcHead.Controls.Add(this.txtInvoiceRef);
            this.grbInvcHead.Controls.Add(this.lblInvoiceRef);
            this.grbInvcHead.Controls.Add(this.cmbInvoiceType);
            this.grbInvcHead.Controls.Add(this.lblInvoiceType);
            this.grbInvcHead.Controls.Add(this.dtpDueDate);
            this.grbInvcHead.Controls.Add(this.dtpInvoiceDate);
            this.grbInvcHead.Controls.Add(this.lblDueDate);
            this.grbInvcHead.Controls.Add(this.lblInvoiceDate);
            this.grbInvcHead.Controls.Add(this.txtLegalNumber);
            this.grbInvcHead.Controls.Add(this.lblLegalNumber);
            this.grbInvcHead.Controls.Add(this.txtInvoiceNum);
            this.grbInvcHead.Controls.Add(this.lblInvoiceNum);
            this.grbInvcHead.Location = new System.Drawing.Point(12, 229);
            this.grbInvcHead.Name = "grbInvcHead";
            this.grbInvcHead.Size = new System.Drawing.Size(1014, 130);
            this.grbInvcHead.TabIndex = 2;
            this.grbInvcHead.TabStop = false;
            this.grbInvcHead.Text = "Encabezado Factura";
            // 
            // grbPrepayment
            // 
            this.grbPrepayment.Controls.Add(this.dateTimePicker1);
            this.grbPrepayment.Controls.Add(this.lblFechapp);
            this.grbPrepayment.Controls.Add(this.txtVlrPrepaidment);
            this.grbPrepayment.Controls.Add(this.lblValor);
            this.grbPrepayment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbPrepayment.Location = new System.Drawing.Point(622, 9);
            this.grbPrepayment.Name = "grbPrepayment";
            this.grbPrepayment.Size = new System.Drawing.Size(384, 60);
            this.grbPrepayment.TabIndex = 19;
            this.grbPrepayment.TabStop = false;
            this.grbPrepayment.Text = "Registro Adelanto";
            this.grbPrepayment.Visible = false;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(202, 20);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(177, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // lblFechapp
            // 
            this.lblFechapp.AutoSize = true;
            this.lblFechapp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechapp.Location = new System.Drawing.Point(154, 23);
            this.lblFechapp.Name = "lblFechapp";
            this.lblFechapp.Size = new System.Drawing.Size(40, 13);
            this.lblFechapp.TabIndex = 2;
            this.lblFechapp.Text = "Fecha:";
            // 
            // txtVlrPrepaidment
            // 
            this.txtVlrPrepaidment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVlrPrepaidment.Location = new System.Drawing.Point(46, 20);
            this.txtVlrPrepaidment.Name = "txtVlrPrepaidment";
            this.txtVlrPrepaidment.Size = new System.Drawing.Size(96, 20);
            this.txtVlrPrepaidment.TabIndex = 1;
            this.txtVlrPrepaidment.Text = "0";
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(6, 24);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(34, 13);
            this.lblValor.TabIndex = 0;
            this.lblValor.Text = "Valor:";
            // 
            // txtInvoiceComment
            // 
            this.txtInvoiceComment.Location = new System.Drawing.Point(622, 75);
            this.txtInvoiceComment.Multiline = true;
            this.txtInvoiceComment.Name = "txtInvoiceComment";
            this.txtInvoiceComment.Size = new System.Drawing.Size(386, 51);
            this.txtInvoiceComment.TabIndex = 18;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(514, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Comentario Factura:";
            // 
            // txtOrderNum
            // 
            this.txtOrderNum.Location = new System.Drawing.Point(333, 26);
            this.txtOrderNum.Name = "txtOrderNum";
            this.txtOrderNum.Size = new System.Drawing.Size(151, 20);
            this.txtOrderNum.TabIndex = 16;
            this.txtOrderNum.Text = "0001";
            // 
            // lblOrderNum
            // 
            this.lblOrderNum.AutoSize = true;
            this.lblOrderNum.Location = new System.Drawing.Point(255, 29);
            this.lblOrderNum.Name = "lblOrderNum";
            this.lblOrderNum.Size = new System.Drawing.Size(73, 13);
            this.lblOrderNum.TabIndex = 15;
            this.lblOrderNum.Text = "Ordern Venta:";
            // 
            // txtInvoiceRef
            // 
            this.txtInvoiceRef.Location = new System.Drawing.Point(333, 77);
            this.txtInvoiceRef.Name = "txtInvoiceRef";
            this.txtInvoiceRef.ReadOnly = true;
            this.txtInvoiceRef.Size = new System.Drawing.Size(151, 20);
            this.txtInvoiceRef.TabIndex = 14;
            this.txtInvoiceRef.Text = "Número Legal a Anular";
            // 
            // lblInvoiceRef
            // 
            this.lblInvoiceRef.AutoSize = true;
            this.lblInvoiceRef.Location = new System.Drawing.Point(252, 80);
            this.lblInvoiceRef.Name = "lblInvoiceRef";
            this.lblInvoiceRef.Size = new System.Drawing.Size(76, 13);
            this.lblInvoiceRef.TabIndex = 13;
            this.lblInvoiceRef.Text = "Nro Legal Ref:";
            // 
            // cmbInvoiceType
            // 
            this.cmbInvoiceType.FormattingEnabled = true;
            this.cmbInvoiceType.Location = new System.Drawing.Point(93, 21);
            this.cmbInvoiceType.Name = "cmbInvoiceType";
            this.cmbInvoiceType.Size = new System.Drawing.Size(151, 21);
            this.cmbInvoiceType.TabIndex = 12;
            this.cmbInvoiceType.SelectedValueChanged += new System.EventHandler(this.cmbInvoiceType_SelectedValueChanged);
            // 
            // lblInvoiceType
            // 
            this.lblInvoiceType.AutoSize = true;
            this.lblInvoiceType.Location = new System.Drawing.Point(6, 26);
            this.lblInvoiceType.Name = "lblInvoiceType";
            this.lblInvoiceType.Size = new System.Drawing.Size(70, 13);
            this.lblInvoiceType.TabIndex = 11;
            this.lblInvoiceType.Text = "Tipo Factura:";
            // 
            // dtpDueDate
            // 
            this.dtpDueDate.Location = new System.Drawing.Point(333, 102);
            this.dtpDueDate.Name = "dtpDueDate";
            this.dtpDueDate.Size = new System.Drawing.Size(151, 20);
            this.dtpDueDate.TabIndex = 10;
            // 
            // dtpInvoiceDate
            // 
            this.dtpInvoiceDate.Location = new System.Drawing.Point(93, 101);
            this.dtpInvoiceDate.Name = "dtpInvoiceDate";
            this.dtpInvoiceDate.Size = new System.Drawing.Size(151, 20);
            this.dtpInvoiceDate.TabIndex = 9;
            // 
            // lblDueDate
            // 
            this.lblDueDate.AutoSize = true;
            this.lblDueDate.Location = new System.Drawing.Point(247, 107);
            this.lblDueDate.Name = "lblDueDate";
            this.lblDueDate.Size = new System.Drawing.Size(74, 13);
            this.lblDueDate.TabIndex = 8;
            this.lblDueDate.Text = "Fecha Vence:";
            // 
            // lblInvoiceDate
            // 
            this.lblInvoiceDate.AutoSize = true;
            this.lblInvoiceDate.Location = new System.Drawing.Point(7, 103);
            this.lblInvoiceDate.Name = "lblInvoiceDate";
            this.lblInvoiceDate.Size = new System.Drawing.Size(79, 13);
            this.lblInvoiceDate.TabIndex = 4;
            this.lblInvoiceDate.Text = "Fecha Factura:";
            // 
            // txtLegalNumber
            // 
            this.txtLegalNumber.Location = new System.Drawing.Point(93, 75);
            this.txtLegalNumber.Name = "txtLegalNumber";
            this.txtLegalNumber.ReadOnly = true;
            this.txtLegalNumber.Size = new System.Drawing.Size(151, 20);
            this.txtLegalNumber.TabIndex = 3;
            // 
            // lblLegalNumber
            // 
            this.lblLegalNumber.AutoSize = true;
            this.lblLegalNumber.Location = new System.Drawing.Point(7, 77);
            this.lblLegalNumber.Name = "lblLegalNumber";
            this.lblLegalNumber.Size = new System.Drawing.Size(56, 13);
            this.lblLegalNumber.TabIndex = 2;
            this.lblLegalNumber.Text = "Nro Legal:";
            // 
            // txtInvoiceNum
            // 
            this.txtInvoiceNum.Location = new System.Drawing.Point(93, 48);
            this.txtInvoiceNum.Name = "txtInvoiceNum";
            this.txtInvoiceNum.ReadOnly = true;
            this.txtInvoiceNum.Size = new System.Drawing.Size(151, 20);
            this.txtInvoiceNum.TabIndex = 1;
            // 
            // lblInvoiceNum
            // 
            this.lblInvoiceNum.AutoSize = true;
            this.lblInvoiceNum.Location = new System.Drawing.Point(7, 51);
            this.lblInvoiceNum.Name = "lblInvoiceNum";
            this.lblInvoiceNum.Size = new System.Drawing.Size(66, 13);
            this.lblInvoiceNum.TabIndex = 0;
            this.lblInvoiceNum.Text = "Nro Factura:";
            // 
            // btnTracer
            // 
            this.btnTracer.Image = global::App_FEDeskot.Properties.Resources.application_form_magnify;
            this.btnTracer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTracer.Location = new System.Drawing.Point(419, 597);
            this.btnTracer.Name = "btnTracer";
            this.btnTracer.Size = new System.Drawing.Size(87, 23);
            this.btnTracer.TabIndex = 19;
            this.btnTracer.Text = "Log Envio";
            this.btnTracer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTracer.UseVisualStyleBackColor = true;
            this.btnTracer.Visible = false;
            this.btnTracer.Click += new System.EventHandler(this.btnTracer_Click);
            // 
            // grbInvDtl
            // 
            this.grbInvDtl.Controls.Add(this.label11);
            this.grbInvDtl.Controls.Add(this.pnlLines);
            this.grbInvDtl.Controls.Add(this.btnAddline);
            this.grbInvDtl.Controls.Add(this.label8);
            this.grbInvDtl.Controls.Add(this.label7);
            this.grbInvDtl.Controls.Add(this.label6);
            this.grbInvDtl.Controls.Add(this.label5);
            this.grbInvDtl.Controls.Add(this.label4);
            this.grbInvDtl.Controls.Add(this.label3);
            this.grbInvDtl.Controls.Add(this.label2);
            this.grbInvDtl.Controls.Add(this.label1);
            this.grbInvDtl.Location = new System.Drawing.Point(12, 363);
            this.grbInvDtl.Name = "grbInvDtl";
            this.grbInvDtl.Size = new System.Drawing.Size(1014, 188);
            this.grbInvDtl.TabIndex = 3;
            this.grbInvDtl.TabStop = false;
            this.grbInvDtl.Text = "Detalle Factura";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(543, 37);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Configuración Descueto";
            // 
            // pnlLines
            // 
            this.pnlLines.AutoScroll = true;
            this.pnlLines.Location = new System.Drawing.Point(0, 51);
            this.pnlLines.Name = "pnlLines";
            this.pnlLines.Size = new System.Drawing.Size(1008, 135);
            this.pnlLines.TabIndex = 9;
            // 
            // btnAddline
            // 
            this.btnAddline.Image = global::App_FEDeskot.Properties.Resources.add;
            this.btnAddline.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAddline.Location = new System.Drawing.Point(92, 14);
            this.btnAddline.Name = "btnAddline";
            this.btnAddline.Size = new System.Drawing.Size(25, 20);
            this.btnAddline.TabIndex = 8;
            this.btnAddline.UseVisualStyleBackColor = true;
            this.btnAddline.Click += new System.EventHandler(this.btnAddline_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nueva Linea:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(886, 37);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Val Impuesto";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(698, 37);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Impuesto";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(428, 37);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Precio Extendido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(366, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cantidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(259, 37);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Precio Unitario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descripción";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Linea";
            // 
            // lblDspDocSubTotal
            // 
            this.lblDspDocSubTotal.AutoSize = true;
            this.lblDspDocSubTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDspDocSubTotal.Location = new System.Drawing.Point(14, 555);
            this.lblDspDocSubTotal.Name = "lblDspDocSubTotal";
            this.lblDspDocSubTotal.Size = new System.Drawing.Size(62, 13);
            this.lblDspDocSubTotal.TabIndex = 14;
            this.lblDspDocSubTotal.Text = "SubTotal:";
            // 
            // lblDocTaxAmt
            // 
            this.lblDocTaxAmt.AutoSize = true;
            this.lblDocTaxAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDocTaxAmt.Location = new System.Drawing.Point(217, 555);
            this.lblDocTaxAmt.Name = "lblDocTaxAmt";
            this.lblDocTaxAmt.Size = new System.Drawing.Size(100, 13);
            this.lblDocTaxAmt.TabIndex = 15;
            this.lblDocTaxAmt.Text = "Impuestos (IVA):";
            // 
            // lblDspDocInvoiceAmt
            // 
            this.lblDspDocInvoiceAmt.AutoSize = true;
            this.lblDspDocInvoiceAmt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDspDocInvoiceAmt.Location = new System.Drawing.Point(467, 554);
            this.lblDspDocInvoiceAmt.Name = "lblDspDocInvoiceAmt";
            this.lblDspDocInvoiceAmt.Size = new System.Drawing.Size(51, 13);
            this.lblDspDocInvoiceAmt.TabIndex = 16;
            this.lblDspDocInvoiceAmt.Text = "TOTAL:";
            // 
            // txtDspDocSubTotal
            // 
            this.txtDspDocSubTotal.Location = new System.Drawing.Point(73, 552);
            this.txtDspDocSubTotal.Name = "txtDspDocSubTotal";
            this.txtDspDocSubTotal.ReadOnly = true;
            this.txtDspDocSubTotal.Size = new System.Drawing.Size(134, 20);
            this.txtDspDocSubTotal.TabIndex = 17;
            // 
            // txtDocTaxAmt
            // 
            this.txtDocTaxAmt.Location = new System.Drawing.Point(323, 552);
            this.txtDocTaxAmt.Name = "txtDocTaxAmt";
            this.txtDocTaxAmt.ReadOnly = true;
            this.txtDocTaxAmt.Size = new System.Drawing.Size(134, 20);
            this.txtDocTaxAmt.TabIndex = 18;
            // 
            // txtDspDocInvoiceAmt
            // 
            this.txtDspDocInvoiceAmt.Location = new System.Drawing.Point(524, 552);
            this.txtDspDocInvoiceAmt.Name = "txtDspDocInvoiceAmt";
            this.txtDspDocInvoiceAmt.ReadOnly = true;
            this.txtDspDocInvoiceAmt.Size = new System.Drawing.Size(134, 20);
            this.txtDspDocInvoiceAmt.TabIndex = 19;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = global::App_FEDeskot.Properties.Resources.cancel;
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(619, 597);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(56, 23);
            this.btnCancelar.TabIndex = 13;
            this.btnCancelar.Text = "Salir";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnGuardar
            // 
            this.btnGuardar.Image = global::App_FEDeskot.Properties.Resources.disk;
            this.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardar.Location = new System.Drawing.Point(251, 597);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(71, 23);
            this.btnGuardar.TabIndex = 12;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // pgbEnvio
            // 
            this.pgbEnvio.Location = new System.Drawing.Point(330, 576);
            this.pgbEnvio.Name = "pgbEnvio";
            this.pgbEnvio.Size = new System.Drawing.Size(258, 17);
            this.pgbEnvio.TabIndex = 20;
            this.pgbEnvio.Visible = false;
            // 
            // ManualInvoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 622);
            this.ControlBox = false;
            this.Controls.Add(this.btnTracer);
            this.Controls.Add(this.pgbEnvio);
            this.Controls.Add(this.txtDspDocInvoiceAmt);
            this.Controls.Add(this.txtDocTaxAmt);
            this.Controls.Add(this.txtDspDocSubTotal);
            this.Controls.Add(this.lblDspDocInvoiceAmt);
            this.Controls.Add(this.lblDocTaxAmt);
            this.Controls.Add(this.lblDspDocSubTotal);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.grbInvDtl);
            this.Controls.Add(this.grbInvcHead);
            this.Controls.Add(this.grbCliente);
            this.Controls.Add(this.grbFcturador);
            this.Name = "ManualInvoice";
            this.Text = "Registro de factura Manual";
            this.Load += new System.EventHandler(this.ManualInvoice_Load);
            this.grbFcturador.ResumeLayout(false);
            this.grbFcturador.PerformLayout();
            this.grbCliente.ResumeLayout(false);
            this.grbCliente.PerformLayout();
            this.grbInvcHead.ResumeLayout(false);
            this.grbInvcHead.PerformLayout();
            this.grbPrepayment.ResumeLayout(false);
            this.grbPrepayment.PerformLayout();
            this.grbInvDtl.ResumeLayout(false);
            this.grbInvDtl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grbFcturador;
        private System.Windows.Forms.GroupBox grbCliente;
        private System.Windows.Forms.GroupBox grbInvcHead;
        private System.Windows.Forms.GroupBox grbInvDtl;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label lblAddress1;
        private System.Windows.Forms.TextBox txtCity;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.Label lblState;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtNit;
        private System.Windows.Forms.Label lblNit;
        private System.Windows.Forms.TextBox txtCompanyID;
        private System.Windows.Forms.Label lblCompanyID;
        private System.Windows.Forms.Label lblCellPhone;
        private System.Windows.Forms.TextBox txtCellPhone;
        private System.Windows.Forms.Label lblEmailClient;
        private System.Windows.Forms.TextBox txtEmailClient;
        private System.Windows.Forms.Label lblAddressClient;
        private System.Windows.Forms.TextBox txtAddressClient;
        private System.Windows.Forms.Label lblCityClient;
        private System.Windows.Forms.TextBox txtCityClient;
        private System.Windows.Forms.Label lblEstateClient;
        private System.Windows.Forms.TextBox txtStateClient;
        private System.Windows.Forms.Label lblNameCliente;
        private System.Windows.Forms.TextBox txtNameCliente;
        private System.Windows.Forms.ComboBox cmbIdentificationType;
        private System.Windows.Forms.Label lblNitCliente;
        private System.Windows.Forms.TextBox txtNitCliente;
        private System.Windows.Forms.Label lblIdentificationType;
        private System.Windows.Forms.ComboBox cmbCurrencyCode;
        private System.Windows.Forms.Label lblCurrencyCode;
        private System.Windows.Forms.Label lblInvoiceDate;
        private System.Windows.Forms.TextBox txtLegalNumber;
        private System.Windows.Forms.Label lblLegalNumber;
        private System.Windows.Forms.TextBox txtInvoiceNum;
        private System.Windows.Forms.Label lblInvoiceNum;
        private System.Windows.Forms.DateTimePicker dtpDueDate;
        private System.Windows.Forms.DateTimePicker dtpInvoiceDate;
        private System.Windows.Forms.Label lblDueDate;
        private System.Windows.Forms.ComboBox cmbInvoiceType;
        private System.Windows.Forms.Label lblInvoiceType;
        private System.Windows.Forms.TextBox txtOrderNum;
        private System.Windows.Forms.Label lblOrderNum;
        private System.Windows.Forms.TextBox txtInvoiceRef;
        private System.Windows.Forms.Label lblInvoiceRef;
        private System.Windows.Forms.Label lblDspDocSubTotal;
        private System.Windows.Forms.Label lblDocTaxAmt;
        private System.Windows.Forms.Label lblDspDocInvoiceAmt;
        private System.Windows.Forms.TextBox txtDspDocSubTotal;
        private System.Windows.Forms.TextBox txtDocTaxAmt;
        private System.Windows.Forms.TextBox txtDspDocInvoiceAmt;
        private System.Windows.Forms.Panel pnlLines;
        private System.Windows.Forms.Button btnAddline;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtInvoiceComment;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnTracer;
        private System.Windows.Forms.ProgressBar pgbEnvio;
        private System.Windows.Forms.ComboBox cmbCliente;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbPais;
        private System.Windows.Forms.Label lblPais;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox grbPrepayment;
        private System.Windows.Forms.TextBox txtVlrPrepaidment;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Label lblFechapp;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
    }
}