﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Referenciar y usar.
using RULES.ARInvoice;
using System.Collections;
using System.Xml;
using System.IO;


namespace App_FEDeskot.MN.Invoice
{
    public partial class ManualInvoice : FormBase
    {
        #region "Constructor"

        public ManualInvoice()
        {
            InitializeComponent();
        }

        #endregion

        #region "Atributos"

        private DataSet _DsManualInvoice;
        private DataTable _DtClientes;
        private XmlDocument XmlInvoiceTemplete;
        private List<InvoiceLine> InvcLines= new List<InvoiceLine>();   
        private Point PointLines = new Point(6, 3);
        private decimal _DspDocSubTotal =0;
        private decimal _DocTaxAmt =0;
        private decimal _DspDocInvoiceAmt =0;
        private string Tracer;
        private string _Error;

        #endregion

        #region "Propiedades"



        #endregion

        #region "Form Load"

        private void ManualInvoice_Load(object sender, EventArgs e)
        {
            try
            {               
                ////1. Cargamos informacion de la compañia.
                //if (!cargafInfoCompany())
                //    throw new Exception(_Error);

                ////2. Cargar Tipos de Identificacion.
                //if (!CargarIdentificationType())
                //    throw new Exception(_Error);

                //if(!BuscarClientes())
                //    throw new Exception(_Error);

                //if (!CodeListCountries())
                //    throw new Exception(_Error);
                
                //if (!CargarTiposModeda())
                //    throw new Exception(_Error);

                //if (!CargarTipoFactura())
                //    throw new Exception();

                //if (!GetConsecutivoFactura())
                //    throw new Exception();

                //if (!AddInvoiceLine())
                //    throw new Exception(_Error);

            }
            catch (Exception ex)
            {
                MensajeErr("Error al cargar configuraciones iniciales: " + ex.Message);
            }
        }        

        #endregion

        #region "Metodos Form"

        private bool cargafInfoCompany()
        {
            XmlDocument xmlCompany = new XmlDocument();            

            try
            {
                string RutaXmlCompany = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\XmlCompany.xml";                
                xmlCompany.Load(RutaXmlCompany);

                //Cargamos Informacion por defecto.
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlCompany.NameTable);
                string query = "Company";

                XmlNode NodoCompany = xmlCompany.SelectSingleNode(query, manager);

                txtCompanyID.Text = NodoCompany["Company"].InnerText;
                txtNombre.Text = NodoCompany["Name"].InnerText;
                txtNit.Text = NodoCompany["StateTaxID"].InnerText;
                txtEstado.Text = NodoCompany["State"].InnerText;
                txtCity.Text = NodoCompany["City"].InnerText;
                txtAddress1.Text = NodoCompany["Address1"].InnerText;

                return true;

            }
            catch (Exception ex)
            {
                _Error = ex.Message;
                return false; 
            }
            finally
            {
                xmlCompany=null;
            }
        }

        private bool CodeListCountries()
        {
            XmlDocument xmlCountries = new XmlDocument();

            try
            {
                string RutaXmlCountries = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\CountriesAlpha2.xml";
                xmlCountries.Load(RutaXmlCountries);

                DataSet ds = new DataSet();
                ds.ReadXml(RutaXmlCountries);

                cmbPais.DataSource = ds.Tables["country"];
                cmbPais.DisplayMember = "name";
                cmbPais.ValueMember = "alpha-2";
                cmbPais.Refresh();
                cmbPais.SelectedValue = "CO";

                return true;

            }
            catch (Exception ex)
            {
                _Error = "No se cargo ClodeList countries Alpha2:" + ex.Message;
                return false;
            }
            finally
            {
                xmlCountries = null;
            }
        }

        private bool BuscarClientes()
        {
            string RutaCustomer = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\Customers.xml";

            try
            {
                //Ds para Ordenar y mostrar clientes.
                DataSet ds = new DataSet();
                ds.ReadXml(RutaCustomer);
                _DtClientes = ds.Tables["Customer"];

                DataRow newItem = _DtClientes.NewRow();
                newItem["Number"] = "X";
                newItem["Name"] = " Seleccione cliente";
                _DtClientes.Rows.Add(newItem);

                cmbCliente.DataSource =_DtClientes;
                cmbCliente.DisplayMember = "Name";
                cmbCliente.ValueMember = "Number";
                cmbCliente.Refresh();
                cmbCliente.SelectedValue = "X";

                return true;

            }
            catch (Exception ex)
            {
                _Error= ex.Message;
                return false;
            }
        }

        private void CargarInfoCliente(string NumberID,string Name)
        {
            try
            {
                if (NumberID == "X")
                    return;

                DataRow [] ClientRow = _DtClientes.Select("Number='"+NumberID+"' and Name='"+Name+"'");

                if(ClientRow!=null && ClientRow.GetLength(0)>0)
                {
                    cmbIdentificationType.SelectedValue = ClientRow[0]["IdentificationType"].ToString();
                    txtNitCliente.Text = ClientRow[0]["Number"].ToString();
                    txtNameCliente.Text = ClientRow[0]["Name"].ToString();
                    cmbPais.SelectedValue = ClientRow[0]["Country"].ToString();
                    txtStateClient.Text = ClientRow[0]["State"].ToString();
                    txtCityClient.Text = ClientRow[0]["City"].ToString();
                    txtAddressClient.Text = ClientRow[0]["Addres"].ToString();
                    txtEmailClient.Text = ClientRow[0]["Email"].ToString();
                    txtCellPhone.Text = ClientRow[0]["CellPhone"].ToString();
                }
            }
            catch (Exception ex)
            {
                MensajeErr("Error al cargar información del cliente: " + ex.Message);                
            }
        }

        private bool CargarIdentificationType()
        {
            try
            {
                DataTable DtIdentificationType = new DataTable();
                DtIdentificationType.Columns.Add("Id", typeof(string));
                DtIdentificationType.Columns.Add("Name", typeof(string));

                DataRow Row1= DtIdentificationType.NewRow();
                Row1["Id"]="X";
                Row1["Name"]="Seleccione";
                DtIdentificationType.Rows.Add(Row1);

                DataRow Row2= DtIdentificationType.NewRow();
                Row2["Id"] = "11";
                Row2["Name"] = "Registro civil";
                DtIdentificationType.Rows.Add(Row2);

                DataRow Row3= DtIdentificationType.NewRow();
                Row3["Id"]="12";
                Row3["Name"]="Tarjeta de identidad";
                DtIdentificationType.Rows.Add(Row3);

                DataRow Row4= DtIdentificationType.NewRow();
                Row4["Id"]="13";
                Row4["Name"]="Cédula de ciudadanía";
                DtIdentificationType.Rows.Add(Row4);

                DataRow Row5= DtIdentificationType.NewRow();
                Row5["Id"]="21";
                Row5["Name"]="Tarjeta de extranjería";
                DtIdentificationType.Rows.Add(Row5);

                DataRow Row6= DtIdentificationType.NewRow();
                Row6["Id"]="22";
                Row6["Name"]="Cédula de extranjería";
                DtIdentificationType.Rows.Add(Row6);

                DataRow Row7= DtIdentificationType.NewRow();
                Row7["Id"]="31";
                Row7["Name"]="NIT";
                DtIdentificationType.Rows.Add(Row7);

                DataRow Row8= DtIdentificationType.NewRow();
                Row8["Id"]="41";
                Row8["Name"]="Pasaporte";
                DtIdentificationType.Rows.Add(Row8);

                DataRow Row9= DtIdentificationType.NewRow();
                Row9["Id"]="42";
                Row9["Name"]="Documento de identificación extranjero";
                DtIdentificationType.Rows.Add(Row9);

                cmbIdentificationType.DataSource = DtIdentificationType;
                cmbIdentificationType.ValueMember = "Id";
                cmbIdentificationType.DisplayMember = "Name";                
                cmbIdentificationType.Refresh();
                cmbIdentificationType.SelectedValue = "X";

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar tipos de Identificación: " + ex.Message;
                return false;
            }

        }

        private bool CargarTiposModeda()
        {
            try
            {
                DataTable DtCurrencyCode = new DataTable();
                DtCurrencyCode.Columns.Add("Id", typeof(string));
                DtCurrencyCode.Columns.Add("Name", typeof(string));

                DataRow Row1 = DtCurrencyCode.NewRow();
                Row1["Id"] = "USD";
                Row1["Name"] = "United States of America, Dollar";
                DtCurrencyCode.Rows.Add(Row1);

                DataRow Row2 = DtCurrencyCode.NewRow();
                Row2["Id"] = "EUR";
                Row2["Name"] = "Euro Member Country";
                DtCurrencyCode.Rows.Add(Row2);

                DataRow Row3 = DtCurrencyCode.NewRow();
                Row3["Id"] = "COP";
                Row3["Name"] = "Colombia, Pesos";
                DtCurrencyCode.Rows.Add(Row3);                                     

                cmbCurrencyCode.DataSource = DtCurrencyCode;
                cmbCurrencyCode.ValueMember = "Id";
                cmbCurrencyCode.DisplayMember = "Name";
                cmbCurrencyCode.Refresh();
                cmbCurrencyCode.SelectedValue = "COP";

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Tipos de Modeda: " + ex.Message;
                return false;
            }

        }

        private bool CargarTipoFactura()
        {
            try
            {
                DataTable DtTipoFactura= new DataTable();
                DtTipoFactura.Columns.Add("Id", typeof(string));
                DtTipoFactura.Columns.Add("Name", typeof(string));

                DataRow Row1 = DtTipoFactura.NewRow();
                Row1["Id"] = "InvoiceType";
                Row1["Name"] = "Factura de Venta";
                DtTipoFactura.Rows.Add(Row1);

                DataRow Row2 = DtTipoFactura.NewRow();
                Row2["Id"] = "CreditNoteType";
                Row2["Name"] = "Nota Crédito";
                DtTipoFactura.Rows.Add(Row2);

                DataRow Row3 = DtTipoFactura.NewRow();
                Row3["Id"] = "DebitNoteType";
                Row3["Name"] = "Nota Debito";
                DtTipoFactura.Rows.Add(Row3);         

                cmbInvoiceType.DataSource = DtTipoFactura;
                cmbInvoiceType.ValueMember = "Id";
                cmbInvoiceType.DisplayMember = "Name";
                cmbInvoiceType.Refresh();
                cmbInvoiceType.SelectedValue = "InvoiceType";

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Tipos de Facturas: " + ex.Message;
                return false;
            }

        }

        private bool GetConsecutivoFactura()
        {
            XmlDocument xmlInvoiceCounter = new XmlDocument();
            XmlDocument xmlLegalNumber = new XmlDocument();

            try
            {
                string RutaXmlSalesTRC = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\InvoiceCounter.xml";
                string RutaXmlLegalNumber = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\LegalNumber.xml";
                xmlInvoiceCounter.Load(RutaXmlSalesTRC);
                xmlLegalNumber.Load(RutaXmlLegalNumber);

                //Cargamos Informacion por defecto.
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlInvoiceCounter.NameTable);
                XmlNamespaceManager managerlegalNum = new XmlNamespaceManager(xmlLegalNumber.NameTable);
                string query = "InvoiceCounter";
                string queryLegalNum = "Resolucion";

                //Numero Interno Factura
                XmlNode NodoCounter = xmlInvoiceCounter.SelectSingleNode(query, manager);
                int Inicio = Convert.ToInt32(NodoCounter["Start"].InnerText);
                int Incremento = Convert.ToInt32(NodoCounter["Increment"].InnerText);

                txtInvoiceNum.Text = (Inicio + Incremento).ToString();

                //Numero Legal Factura.
                XmlNode NodoLegalNum = xmlLegalNumber.SelectSingleNode(queryLegalNum, managerlegalNum);
                string Prefijo = NodoLegalNum["Prefijo"].InnerText;
                int RangoIncial = Convert.ToInt32(NodoLegalNum["RangoInicial"].InnerText);
                int RangoFinal = Convert.ToInt32(NodoLegalNum["RangoFinal"].InnerText);
                int IncrementoLegalNum = Convert.ToInt32(NodoLegalNum["Incremento"].InnerText);

                int NextlegalNum = RangoIncial + IncrementoLegalNum;

                if (NextlegalNum > RangoFinal)
                    throw new Exception("Se agoto el consecutivo de números legales para la resolucion configurada.");

                txtLegalNumber.Text = Prefijo + NextlegalNum;
                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Listado de impuestos: " + ex.Message;
                return false;
            }
            finally
            {
                xmlInvoiceCounter = null;
            }

        }

        private void ActualizarConsecutivoFactura()
        {
            XmlDocument xmlInvoiceCounter = new XmlDocument();
            XmlDocument xmlLegalNumber = new XmlDocument();

            try
            {
                string RutaXmlInvoiceCounter = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\InvoiceCounter.xml";
                string RutaXmlLegalNumber = AppDomain.CurrentDomain.BaseDirectory.ToString() + "ManualInvoiceSettings\\LegalNumber.xml";
                xmlInvoiceCounter.Load(RutaXmlInvoiceCounter);
                xmlLegalNumber.Load(RutaXmlLegalNumber);

                
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlInvoiceCounter.NameTable);
                XmlNamespaceManager managerLegalNum = new XmlNamespaceManager(xmlLegalNumber.NameTable);
                string query = "InvoiceCounter";
                string queryLegalnum = "Resolucion";

                //Consecutivo Interno Factura.
                XmlNode NodoCounter = xmlInvoiceCounter.SelectSingleNode(query, manager);
                NodoCounter["Start"].InnerText = txtInvoiceNum.Text;

                xmlInvoiceCounter.Save(RutaXmlInvoiceCounter);

                //Incremento de Numero Legal.
                XmlNode NodoLegalNum = xmlLegalNumber.SelectSingleNode(queryLegalnum, managerLegalNum);
                NodoLegalNum["Incremento"].InnerText = (Convert.ToInt32(NodoLegalNum["Incremento"].InnerText) + 1).ToString();

                xmlLegalNumber.Save(RutaXmlLegalNumber);

            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Listado de impuestos: " + ex.Message;
            }
            finally
            {
                xmlInvoiceCounter = null;
            }
        }

        private bool AddInvoiceLine()
        {
            try
            {                       
                int VLeng = 24; //Position on Y to Increment.

                //Default Lines.
                InvoiceLine Line1 = new InvoiceLine();
                Line1.SendTotalInvoice += new InvoiceLine.TotalInvoice(TotalInvoice);                                

                //Agregamos Linea a la lista de lineas..
                InvcLines.Add(Line1);
                Line1.Line = InvcLines.Count;                

                //Agregamos linea al contenedor grbInvDtl                              
                pnlLines.Controls.Add(Line1);

                //Fijamos Location para la linea.
                if (grbInvDtl.Controls.Count <= 0)
                {
                    Line1.Location = new System.Drawing.Point(6, 14);
                    PointLines.Y += VLeng + 2;
                }
                else
                {
                    Line1.Location = new System.Drawing.Point(6, PointLines.Y);
                    PointLines.Y += VLeng + 2;
                }                

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al cargar Lineas de factura por defecto: " + ex.Message;
                return false;
            }

        }

        public void TotalInvoice()
        {
            try
            {             
                _DspDocSubTotal =0;               
                _DocTaxAmt =0;
                _DspDocInvoiceAmt =0;

                foreach(InvoiceLine Line in InvcLines)
                {
                    _DspDocSubTotal += Line.DspDocExtPrice - Line.DspDocLessDiscount;                    
                    _DocTaxAmt += Line.TaxAmt;
                }

                txtDspDocSubTotal.Text = string.Format("{0:N4}", _DspDocSubTotal);
                txtDocTaxAmt.Text = string.Format("{0:N4}", _DocTaxAmt);
                _DspDocInvoiceAmt = (_DspDocSubTotal + _DocTaxAmt);
                txtDspDocInvoiceAmt.Text = string.Format("{0:N4}", _DspDocInvoiceAmt);                
            }
            catch (Exception ex)
            {
                MensajeErr("Error al calcular Total de facutra: "+ex.Message);
            }
        }

        //1. Informacion de Cliente.
        public bool ValidarCutomerInfo()
        {
            try
            {
                if(cmbIdentificationType.SelectedValue.ToString()=="X")
                {
                    _Error = "Seleccione un tipo de Identificación valido para el cliente";
                    return false;
                }

                if (!ValidarNumericoSTR(txtNitCliente.Text))
                {
                    _Error = "Por favor ingrese un NIT valido sin digitó de verificación";
                    return false;
                }

                if (string.IsNullOrEmpty(txtStateClient.Text))
                {
                    _Error = "Ingrese un departamento valido";
                    return false;
                }

                if (string.IsNullOrEmpty(txtCityClient.Text))
                {
                    _Error = "Ingrese una ciudad valida";
                    return false;
                }

                if (string.IsNullOrEmpty(txtAddressClient.Text))
                {
                    _Error = "Ingresa una direccion del cliente valida";
                    return false;
                }
              
                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al validar CutomerInfo: " + ex.Message;
                return false;               
            }
        }

        //2. Informacion Encabezado Factura.
        public bool ValidarInvcHeadInfo()
        {
            try
            {
                if(cmbInvoiceType.SelectedValue.ToString()=="X")
                {
                    _Error = "Selecciona Tipo de factura Valido";
                    cmbInvoiceType.Focus();
                    return false;
                }

                if (string.IsNullOrEmpty(txtLegalNumber.Text))
                {
                    _Error = "Ingresa un Número legal valido";
                    txtLegalNumber.Focus();
                    return false;
                }

                if (cmbInvoiceType.SelectedValue.ToString() == "CreditNoteType")
                {
                    if (string.IsNullOrEmpty(txtInvoiceRef.Text))
                    {
                        _Error = "Ingresa el Número legal de referencia a anular con Nota Credito";
                        txtInvoiceRef.Focus();
                        return false;
                    }
                }

                if (!ValidarNumericoSTR(txtVlrPrepaidment.Text))
                {
                    MensajeWar("Ingresa un valor de adelanto valido para la factura.");                    
                    txtVlrPrepaidment.Focus();
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al validar InvcHeadInfo: "+ex.Message;
                return false;
            }
        }

        //3. Informacion Linea Factura.
        public bool ValidarInvcLine(int Index)
        {
            try
            {
                if(string.IsNullOrEmpty(InvcLines[Index].PartDescription))
                {
                    _Error = "Ingrese descripción de la parte valida en la Linea: "+InvcLines[Index].Line;
                    return false;
                }

                if (InvcLines[Index].UnitPrice<=0)
                {
                    _Error = "Ingrese un precio Unitario valido en la Linea: " + InvcLines[Index].Line;
                    return false;
                }

                if (InvcLines[Index].SellingShipQty <= 0)
                {
                    _Error = "Ingrese una cantidad a vender valida en la Linea: " + InvcLines[Index].Line;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error validando informacion de la Linea " + InvcLines[Index].Line + ": " + ex.Message;
                return false;
            }

        }

        private bool EnviarFacturaToBTW()
        {            
            ARInvoice objInvoice = new ARInvoice();
            //MessageBox.Show(Tracer);
            Tracer = string.Empty;

            try
            {
                objInvoice.Company = txtCompanyID.Text;
                objInvoice.NIT = txtNit.Text;
                objInvoice.InvoiceType = cmbInvoiceType.SelectedValue.ToString();
                objInvoice.XmlARInvoice = XmlInvoiceTemplete.InnerXml;
           
                if (!objInvoice.EnviarFacturaManualToBTW(ref Tracer))
                    throw new Exception(objInvoice.Error);
                


                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al Enviar factura electrónica a BTW: " + ex.Message;                
                return false;
            }
            finally
            {
                objInvoice = null;
            }            
        }

        #endregion

        #region "Construir Factura Manual"

        private void SendInvoiceTest()
        {
            try
            {
                Tracer += "Inicia construccion de factura Electroncia:\n";

                Tracer += "1. Cargando XmlTemplate factura:\n";
                string RutaToSaveInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice\\XmlArIncoice_" + txtLegalNumber.Text + ".xml";
                string RutaXmlStandar = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice\\PruebaXML-81-1.xml";
                XmlInvoiceTemplete = new XmlDocument();
                XmlInvoiceTemplete.Load(RutaXmlStandar);
                //Tracer += "XmlTemplate factura cargado OK\n\n";

                //pgbEnvio.Visible = true;
                //pgbEnvio.Value = 10;

                //Tracer += "2. AddInfoCompany:\n";
                //if (!AddInfoCompany(ref XmlInvoiceTemplete))
                //    throw new Exception(_Error);
                //Tracer += "AddInfoCompany OK\n\n";

                //Tracer += "3. AddInfoCustomer:\n";
                //pgbEnvio.Value = 20;
                //if (!AddInfoCustomer())
                //    throw new Exception(_Error);
                //Tracer += "AddInfoCustomer OK\n\n";

                //Tracer += "4. AddInfoSalesTRC:\n";
                //pgbEnvio.Value = 30;
                //if (!AddInfoSalesTRC())
                //    throw new Exception(_Error);
                //Tracer += "AddInfoSalesTRC OK\n\n";

                //Tracer += "5. AddInfoInvcHead:\n";
                //pgbEnvio.Value = 40;
                //if (!AddInfoInvcHead())
                //    throw new Exception(_Error);
                //Tracer += "AddInfoInvcHead OK\n\n";

                //Tracer += "6. AddInfoInvcDtl:\n";
                //pgbEnvio.Value = 50;
                //if (!AddInfoInvcDtl())
                //    throw new Exception(_Error);
                //Tracer += "AddInfoInvcDtl OK\n\n";


                //Tracer += "7. Guardamos factura Manual:\n";
                ////Guardamos factura Manual.
                //pgbEnvio.Value = 60;
                //XmlInvoiceTemplete.Save(RutaToSaveInvoice);
                //Tracer += "Factura Manual Guardada OK\n\n";

                //Enviar la Factura Manual a BTWServices.
                Tracer += "8. EnviarFacturaToBTW:\n";
                pgbEnvio.Value = 70;
                if (!EnviarFacturaToBTW())
                    throw new Exception(_Error);
                Tracer += "EnviarFacturaToBTW OK\n\n";

                //Actualizamos consecutivo de factura.                
                pgbEnvio.Value = 100;
                ActualizarConsecutivoFactura();

                MensajeOK("Factura almacenada y enviada a la DIAN con éxito.");
                btnGuardar.Enabled = false;
            }
            catch (Exception ex)
            {
                MensajeWar("Error al construir XML de factura manual para enviar a la DIAN: " + ex.Message);
            }
            finally
            {
                pgbEnvio.Visible = false;
                pgbEnvio.Value = 0;
                btnTracer.Visible = true;
            }
        }        

        private void BuildManualInvoice()
        {
            try
            {
                Tracer += "Inicia construccion de factura Electroncia:\n";

                Tracer += "1. Cargando XmlTemplate factura:\n";
                string RutaToSaveInvoice = AppDomain.CurrentDomain.BaseDirectory.ToString() + "XmlInvoice\\XmlArIncoice_"+txtLegalNumber.Text+".xml";
                string RutaXmlStandar = AppDomain.CurrentDomain.BaseDirectory.ToString() + "xmlStandarBTW\\XmlStandarBTW.xml";
                XmlInvoiceTemplete = new XmlDocument();
                XmlInvoiceTemplete.Load(RutaXmlStandar);
                Tracer += "XmlTemplate factura cargado OK\n\n";

                pgbEnvio.Visible = true;
                pgbEnvio.Value = 10;

                Tracer += "2. AddInfoCompany:\n";
                if (!AddInfoCompany(ref XmlInvoiceTemplete))
                    throw new Exception(_Error);
                Tracer += "AddInfoCompany OK\n\n";

                Tracer += "3. AddInfoCustomer:\n";
                pgbEnvio.Value = 20;
                if (!AddInfoCustomer())
                    throw new Exception(_Error);
                Tracer += "AddInfoCustomer OK\n\n";

                Tracer += "4. AddInfoSalesTRC:\n";
                pgbEnvio.Value = 30;
                if (!AddInfoSalesTRC())
                    throw new Exception(_Error);
                Tracer += "AddInfoSalesTRC OK\n\n";

                Tracer += "5. AddInfoInvcHead:\n";
                pgbEnvio.Value = 40;
                if (!AddInfoInvcHead())
                    throw new Exception(_Error);
                Tracer += "AddInfoInvcHead OK\n\n";

                Tracer += "6. AddInfoInvcDtl:\n";
                pgbEnvio.Value = 50;
                if (!AddInfoInvcDtl())
                    throw new Exception(_Error);
                Tracer += "AddInfoInvcDtl OK\n\n";


                Tracer += "7. Guardamos factura Manual:\n";
                //Guardamos factura Manual.
                pgbEnvio.Value = 60;
                XmlInvoiceTemplete.Save(RutaToSaveInvoice);
                Tracer += "Factura Manual Guardada OK\n\n";

                //Enviar la Factura Manual a BTWServices.
                Tracer += "8. EnviarFacturaToBTW:\n";
                pgbEnvio.Value = 70;
                if(!EnviarFacturaToBTW())
                    throw new Exception(_Error);
                Tracer += "EnviarFacturaToBTW OK\n\n";

                //Actualizamos consecutivo de factura.                
                pgbEnvio.Value = 100;
                ActualizarConsecutivoFactura();

                MensajeOK("Factura almacenada y enviada a la DIAN con éxito.");
                btnGuardar.Enabled = false;
            }
            catch (Exception ex)
            {
                MensajeWar("Error al construir XML de factura manual para enviar a la DIAN: "+ex.Message);
            }
            finally 
            {
                pgbEnvio.Visible = false;
                pgbEnvio.Value = 0;
                btnTracer.Visible = true;
            }
        }        

        //1. Informacion de la Compañia.
        private bool AddInfoCompany(ref XmlDocument XmlInvoiceTemplete)
        {
            try
            {
                XmlNamespaceManager manager = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query = "ARInvoiceDataSet/Company";

                XmlNode NodoCompany = XmlInvoiceTemplete.SelectSingleNode(query, manager);

                NodoCompany["Company"].InnerText = txtCompanyID.Text;
                NodoCompany["StateTaxID"].InnerText = txtNit.Text;
                NodoCompany["Name"].InnerText = txtNombre.Text;
                NodoCompany["State"].InnerText = txtEstado.Text;
                NodoCompany["City"].InnerText = txtCity.Text;
                NodoCompany["Address1"].InnerText = txtAddress1.Text;
                
                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al adicionar información de compañia a la factura manual: " + ex.Message;                
                return false;
            }

        }

        //2. Informacion del Cliente (Adquiriente).
        private bool AddInfoCustomer()
        {
            try
            {
                if(!ValidarCutomerInfo())                    
                    return false;                

                XmlNamespaceManager manager = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query = "ARInvoiceDataSet/Customer";

                XmlNode NodoCustomer = XmlInvoiceTemplete.SelectSingleNode(query, manager);


                //Informacion del Cliente.
                NodoCustomer["Company"].InnerText = txtCompanyID.Text;
                NodoCustomer["CustID"].InnerText = txtNitCliente.Text;
                NodoCustomer["CustNum"].InnerText = txtNitCliente.Text;
                NodoCustomer["ResaleID"].InnerText = txtNitCliente.Text;
                NodoCustomer["Name"].InnerText = txtNameCliente.Text;
                NodoCustomer["State"].InnerText = txtStateClient.Text;
                NodoCustomer["City"].InnerText = txtCityClient.Text;
                NodoCustomer["Address1"].InnerText = txtAddressClient.Text;
                NodoCustomer["EMailAddress"].InnerText = txtEmailClient.Text;
                NodoCustomer["PhoneNum"].InnerText = txtCellPhone.Text;
                NodoCustomer["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();
                NodoCustomer["Address2"].InnerText = string.Empty;
                NodoCustomer["Address3"].InnerText = string.Empty;

                //Informacion del OneTime.
                XmlNamespaceManager manager2 = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query2 = "ARInvoiceDataSet/COOneTime";
                XmlNode NodoCOOneTime = XmlInvoiceTemplete.SelectSingleNode(query2, manager2);

                NodoCOOneTime["Company"].InnerText = txtCompanyID.Text;
                NodoCOOneTime["IdentificationType"].InnerText = cmbIdentificationType.SelectedValue.ToString();
                NodoCOOneTime["COOneTimeID"].InnerText = txtNitCliente.Text;
                NodoCOOneTime["CompanyName"].InnerText = txtNameCliente.Text;
                NodoCOOneTime["CountryCode"].InnerText = "CO";                

                return true;
            }
            catch (Exception ex)
            {
                _Error="Error al adicionar información de compañia a la factura manual: " + ex.Message;                
                return false;
            }

        }

        //3. Informacion de Impuestos SalesTRC
        private bool AddInfoSalesTRC()
        {
            try
            {
                DataTable DtSalesTRC = InvcLines[0].DtSalesTRC;
                XmlNamespaceManager manager = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query = "ARInvoiceDataSet/SalesTRC";

                XmlNode NodoSalesTRC = XmlInvoiceTemplete.SelectSingleNode(query, manager);

                if(DtSalesTRC !=null && DtSalesTRC.Rows.Count>0)
                {
                    foreach(DataRow RowTax in DtSalesTRC.Rows)
                    {
                        //Actualizamos primer registro.
                        if(DtSalesTRC.Rows.IndexOf(RowTax)==0)
                        {
                            NodoSalesTRC["Company"].InnerText = txtCompanyID.Text;
                            NodoSalesTRC["RateCode"].InnerText = RowTax["Id"].ToString();
                            NodoSalesTRC["TaxCode"].InnerText = RowTax["TaxCode"].ToString();
                            NodoSalesTRC["Description"].InnerText = RowTax["Name"].ToString();
                            NodoSalesTRC["IdImpDIAN_c"].InnerText = RowTax["IdImpDIAN_c"].ToString();
                        }
                        //Inserto Nuevo registro de Impuesto a la factura manual.
                        else
                        {
                            XmlNode NewNodeSalesTRC = NodoSalesTRC.Clone();

                            //DataRow NewTax = _DsManualInvoice.Tables["SalesTRC"].NewRow();
                            NewNodeSalesTRC["Company"].InnerText = txtCompanyID.Text; ;
                            NodoSalesTRC["RateCode"].InnerText = RowTax["Id"].ToString();
                            NewNodeSalesTRC["TaxCode"].InnerText = RowTax["TaxCode"].ToString();
                            NewNodeSalesTRC["Description"].InnerText = RowTax["Name"].ToString();
                            NewNodeSalesTRC["IdImpDIAN_c"].InnerText = RowTax["IdImpDIAN_c"].ToString();
                            XmlInvoiceTemplete.DocumentElement.InsertAfter(NewNodeSalesTRC, NodoSalesTRC); 
                        }                                                
                    }

                }
                
                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al adicionar información de compañia a la factura manual: " + ex.Message;                
                return false;
            }
        }

        //4. Informacion Encabezado de Factura.
        private bool AddInfoInvcHead()
        {
            try
            {
                if (!ValidarInvcHeadInfo())            
                    return false;

                XmlNamespaceManager manager = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query = "ARInvoiceDataSet/InvcHead";
                string queryPrepayment = "ARInvoiceDataSet/PrepaidPayment";

                XmlNode NodoInvcHead = XmlInvoiceTemplete.SelectSingleNode(query, manager);
                XmlNode NodoPrepayment = XmlInvoiceTemplete.SelectSingleNode(queryPrepayment, manager);

                //Informacion del Cliente.
                NodoInvcHead["Company"].InnerText = txtCompanyID.Text;
                NodoInvcHead["InvoiceType"].InnerText = cmbInvoiceType.SelectedValue.ToString();
                NodoInvcHead["InvoiceNum"].InnerText = txtInvoiceNum.Text;
                NodoInvcHead["LegalNumber"].InnerText = txtLegalNumber.Text;
                NodoInvcHead["InvoiceRef"].InnerText = txtInvoiceRef.Text == "Número Legal a Anular" ? "" : txtInvoiceRef.Text;
                NodoInvcHead["ContactName"].InnerText = txtNameCliente.Text;
                NodoInvcHead["InvoiceDate"].InnerText = string.Format("{0:MM/dd/yyyy HH:mm:ss}", Convert.ToDateTime(dtpInvoiceDate.Value));
                NodoInvcHead["DueDate"].InnerText = string.Format("{0:MM/dd/yyyy HH:mm:ss}", Convert.ToDateTime(dtpDueDate.Value));
                NodoInvcHead["OrderNum"].InnerText = txtOrderNum.Text;
                NodoInvcHead["DspDocSubTotal"].InnerText =_DspDocSubTotal.ToString();
                NodoInvcHead["DocTaxAmt"].InnerText = _DocTaxAmt.ToString();
                NodoInvcHead["DocWHTaxAmt"].InnerText = "0";
                NodoInvcHead["DspDocInvoiceAmt"].InnerText = String.Format("{0:0.###}", _DspDocInvoiceAmt);//txtDspDocInvoiceAmt.Text;
                NodoInvcHead["InvoiceComment"].InnerText = txtInvoiceComment.Text;
                NodoInvcHead["CurrencyCodeCurrencyID"].InnerText = cmbCurrencyCode.SelectedValue.ToString();
                NodoInvcHead["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();         

                //Agregamos Prepayment.
                NodoPrepayment["PaidAmount"].InnerText = txtVlrPrepaidment.Text;
                NodoPrepayment["PaidDate"].InnerText = string.Format("{0:yyyy-MM-dd}", Convert.ToDateTime(dtpDueDate.Value));
                //NodoPrepayment["PaidTime"].InnerText = "";                

                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al adicionar información de compañia a la factura manual: " + ex.Message;
                return false;
            }
        }

        //4. Informacion Lineas de Factura.
        private bool AddInfoInvcDtl()
        {
            try
            {                
                XmlNamespaceManager manager = new XmlNamespaceManager(XmlInvoiceTemplete.NameTable);
                string query = "ARInvoiceDataSet/InvcDtl";
                string queryTax = "ARInvoiceDataSet/InvcTax";

                XmlNode NodoInvcDtl = XmlInvoiceTemplete.SelectSingleNode(query, manager);
                XmlNode NodoInvcTax = XmlInvoiceTemplete.SelectSingleNode(queryTax, manager);

                //Informacion del Cliente.
                foreach(InvoiceLine Line in InvcLines)
                {
                    if (!ValidarInvcLine(Line.Line-1))
                        return false;

                    //Primera Linea actualizamos
                    if (Line.Line == 1)
                    {
                        //Linea de Factura
                        NodoInvcDtl["Company"].InnerText = txtCompanyID.Text;
                        NodoInvcDtl["InvoiceNum"].InnerText = txtInvoiceNum.Text;
                        NodoInvcDtl["InvoiceLine"].InnerText = Line.Line.ToString();
                        NodoInvcDtl["PartNum"].InnerText = Line.Line.ToString();
                        NodoInvcDtl["LineDesc"].InnerText = Line.PartDescription;
                        NodoInvcDtl["PartNumPartDescription"].InnerText = Line.PartDescription;
                        NodoInvcDtl["SellingShipQty"].InnerText = Line.SellingShipQty.ToString();
                        NodoInvcDtl["UnitPrice"].InnerText = Line.UnitPrice.ToString();
                        NodoInvcDtl["DocUnitPrice"].InnerText = Line.UnitPrice.ToString();
                        NodoInvcDtl["DspDocExtPrice"].InnerText = Line.DspDocExtPrice.ToString();
                        //NodoInvcDtl["DspDocExtPrice"].InnerText = Line.DspDocExtPrice.ToString();
                        NodoInvcDtl["DocExtPrice"].InnerText = Line.DspDocExtPrice.ToString();

                        NodoInvcDtl["DiscountPercent"].InnerText = Line.DiscountPercent.ToString();
                        NodoInvcDtl["Discount"].InnerText=Line.DspDocLessDiscount.ToString();
                        NodoInvcDtl["DocDiscount"].InnerText = Line.DspDocLessDiscount.ToString();
                        NodoInvcDtl["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();

                        //Impuesto.
                        NodoInvcTax["Company"].InnerText = txtCompanyID.Text;
                        NodoInvcTax["InvoiceNum"].InnerText = txtInvoiceNum.Text;
                        NodoInvcTax["InvoiceLine"].InnerText = Line.Line.ToString();
                        NodoInvcTax["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();
                        NodoInvcTax["RateCode"].InnerText = Line.RateCode;
                        NodoInvcTax["DocTaxableAmt"].InnerText = Line.DspDocExtPrice.ToString();
                        NodoInvcTax["TaxAmt"].InnerText = String.Format("{0:0.###}", Line.TaxAmt); //Line.TaxAmt.ToString();
                        NodoInvcTax["DocTaxAmt"].InnerText = String.Format("{0:0.###}", Line.TaxAmt); //Line.TaxAmt.ToString();
                        NodoInvcTax["Percent"].InnerText = Line.Percent.ToString();
                    }
                    //Insertamos nueva Linea de factura.
                    else
                    {                        
                        //Linea de Factura                        
                        XmlNode NewNodoInvcDtl = NodoInvcDtl.Clone();
                        NewNodoInvcDtl["Company"].InnerText = txtCompanyID.Text;
                        NewNodoInvcDtl["InvoiceNum"].InnerText = txtInvoiceNum.Text;
                        NewNodoInvcDtl["InvoiceLine"].InnerText = Line.Line.ToString();
                        NewNodoInvcDtl["PartNum"].InnerText = Line.Line.ToString();
                        NewNodoInvcDtl["LineDesc"].InnerText = Line.PartDescription;
                        NewNodoInvcDtl["PartNumPartDescription"].InnerText = Line.PartDescription;
                        NewNodoInvcDtl["SellingShipQty"].InnerText = Line.SellingShipQty.ToString();
                        NewNodoInvcDtl["UnitPrice"].InnerText = Line.UnitPrice.ToString();
                        NewNodoInvcDtl["DocUnitPrice"].InnerText = Line.UnitPrice.ToString();
                        NewNodoInvcDtl["DspDocExtPrice"].InnerText = Line.DspDocExtPrice.ToString();                                             
                        NewNodoInvcDtl["DocExtPrice"].InnerText = Line.DspDocExtPrice.ToString();

                        NewNodoInvcDtl["DiscountPercent"].InnerText = Line.DiscountPercent.ToString();
                        NewNodoInvcDtl["Discount"].InnerText = Line.DspDocLessDiscount.ToString();
                        NewNodoInvcDtl["DocDiscount"].InnerText = Line.DspDocLessDiscount.ToString();
                        NewNodoInvcDtl["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();                       
                        XmlInvoiceTemplete.DocumentElement.InsertAfter(NewNodoInvcDtl, NodoInvcDtl);

                        //Impuesto.                        
                        XmlNode NewNodoInvcTax = NodoInvcTax.Clone();
                        NewNodoInvcTax["Company"].InnerText = txtCompanyID.Text;
                        NewNodoInvcTax["InvoiceNum"].InnerText = txtInvoiceNum.Text;
                        NewNodoInvcTax["InvoiceLine"].InnerText = Line.Line.ToString();
                        NewNodoInvcTax["CurrencyCode"].InnerText = cmbCurrencyCode.SelectedValue.ToString();
                        NewNodoInvcTax["RateCode"].InnerText = Line.RateCode;
                        NewNodoInvcTax["DocTaxableAmt"].InnerText = Line.DspDocExtPrice.ToString();
                        NewNodoInvcTax["TaxAmt"].InnerText = String.Format("{0:0.###}", Line.TaxAmt); //Line.TaxAmt.ToString();
                        NewNodoInvcTax["DocTaxAmt"].InnerText = String.Format("{0:0.###}", Line.TaxAmt); //Line.TaxAmt.ToString();
                        NewNodoInvcTax["Percent"].InnerText = Line.Percent.ToString();
                        XmlInvoiceTemplete.DocumentElement.InsertAfter(NewNodoInvcTax, NodoInvcTax);                        
                    }
                }
                
                return true;
            }
            catch (Exception ex)
            {
                _Error = "Error al adicionar información de compañia a la factura manual: " + ex.Message;
                return false;
            }
        }

        #endregion

        #region "Eventos Controles"

        private void cmbCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarInfoCliente(cmbCliente.SelectedValue.ToString(), cmbCliente.Text);
        }

        private void btnTracer_Click(object sender, EventArgs e)
        {
            try
            {
                PopupError objPopup = new PopupError();
                objPopup.Titulo = "Log de envio factura a BTW y la DIAN";
                objPopup.Mensaje = Tracer;
                objPopup.ShowDialog();
            }
            catch (Exception)
            {

            }
        }

        private void btnAddline_Click(object sender, EventArgs e)
        {
            if (!AddInvoiceLine())
                MensajeErr(_Error);
        }

        private void cmbInvoiceType_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbInvoiceType.SelectedValue.ToString() == "CreditNoteType")
            {
                txtInvoiceRef.ReadOnly = false;
                txtInvoiceRef.Text = string.Empty;
                txtInvoiceRef.Focus();
            }
            else
            {
                grbPrepayment.Visible = true;
                txtInvoiceRef.ReadOnly = true;
                txtInvoiceRef.Text = "Número Legal a Anular";
            }
        }      

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if(MensajeQuestion("¿Esta seguro(a) de guardar esta factura y enviarla a la DIAN?"))
            {
                //BuildManualInvoice();            
                SendInvoiceTest();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();            
        }

        #endregion                                                   
                
    }
}
