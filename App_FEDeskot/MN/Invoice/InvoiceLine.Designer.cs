﻿namespace App_FEDeskot.MN.Invoice
{
    partial class InvoiceLine
    {
        /// <summary> 
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar 
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInvoiceLine = new System.Windows.Forms.Label();
            this.txtPartNumPartDescription = new System.Windows.Forms.TextBox();
            this.txtUnitPrice = new System.Windows.Forms.TextBox();
            this.txtCantidad = new System.Windows.Forms.TextBox();
            this.txtDspDocExtPrice = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbImpuesto = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTaxAmt = new System.Windows.Forms.TextBox();
            this.chkDescPercent = new System.Windows.Forms.CheckBox();
            this.txtPercentDesc = new System.Windows.Forms.TextBox();
            this.txtDspDocLessDiscount = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblInvoiceLine
            // 
            this.lblInvoiceLine.AutoSize = true;
            this.lblInvoiceLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInvoiceLine.Location = new System.Drawing.Point(4, 4);
            this.lblInvoiceLine.Name = "lblInvoiceLine";
            this.lblInvoiceLine.Size = new System.Drawing.Size(18, 13);
            this.lblInvoiceLine.TabIndex = 0;
            this.lblInvoiceLine.Text = "-1";
            // 
            // txtPartNumPartDescription
            // 
            this.txtPartNumPartDescription.Location = new System.Drawing.Point(33, 1);
            this.txtPartNumPartDescription.Name = "txtPartNumPartDescription";
            this.txtPartNumPartDescription.Size = new System.Drawing.Size(205, 20);
            this.txtPartNumPartDescription.TabIndex = 1;
            // 
            // txtUnitPrice
            // 
            this.txtUnitPrice.Location = new System.Drawing.Point(256, 1);
            this.txtUnitPrice.Name = "txtUnitPrice";
            this.txtUnitPrice.Size = new System.Drawing.Size(100, 20);
            this.txtUnitPrice.TabIndex = 2;
            this.txtUnitPrice.TextChanged += new System.EventHandler(this.txtUnitPrice_TextChanged);
            // 
            // txtCantidad
            // 
            this.txtCantidad.Location = new System.Drawing.Point(363, 1);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(39, 20);
            this.txtCantidad.TabIndex = 3;
            this.txtCantidad.TextChanged += new System.EventHandler(this.txtCantidad_TextChanged);
            // 
            // txtDspDocExtPrice
            // 
            this.txtDspDocExtPrice.Location = new System.Drawing.Point(425, 1);
            this.txtDspDocExtPrice.Name = "txtDspDocExtPrice";
            this.txtDspDocExtPrice.ReadOnly = true;
            this.txtDspDocExtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtDspDocExtPrice.TabIndex = 10;
            this.txtDspDocExtPrice.TextChanged += new System.EventHandler(this.txtDspDocExtPrice_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(243, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(14, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "$";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(410, 4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(14, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "$";
            // 
            // cmbImpuesto
            // 
            this.cmbImpuesto.FormattingEnabled = true;
            this.cmbImpuesto.Location = new System.Drawing.Point(708, 0);
            this.cmbImpuesto.Name = "cmbImpuesto";
            this.cmbImpuesto.Size = new System.Drawing.Size(152, 21);
            this.cmbImpuesto.TabIndex = 5;
            this.cmbImpuesto.SelectedValueChanged += new System.EventHandler(this.cmbImpuesto_SelectedValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(866, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "$";
            // 
            // txtTaxAmt
            // 
            this.txtTaxAmt.Location = new System.Drawing.Point(882, 0);
            this.txtTaxAmt.Name = "txtTaxAmt";
            this.txtTaxAmt.ReadOnly = true;
            this.txtTaxAmt.Size = new System.Drawing.Size(101, 20);
            this.txtTaxAmt.TabIndex = 8;
            // 
            // chkDescPercent
            // 
            this.chkDescPercent.AutoSize = true;
            this.chkDescPercent.Location = new System.Drawing.Point(572, 5);
            this.chkDescPercent.Name = "chkDescPercent";
            this.chkDescPercent.Size = new System.Drawing.Size(15, 14);
            this.chkDescPercent.TabIndex = 10;
            this.chkDescPercent.UseVisualStyleBackColor = true;
            this.chkDescPercent.CheckedChanged += new System.EventHandler(this.chkDescPercent_CheckedChanged);
            // 
            // txtPercentDesc
            // 
            this.txtPercentDesc.Location = new System.Drawing.Point(591, 1);
            this.txtPercentDesc.Name = "txtPercentDesc";
            this.txtPercentDesc.ReadOnly = true;
            this.txtPercentDesc.Size = new System.Drawing.Size(26, 20);
            this.txtPercentDesc.TabIndex = 11;
            this.txtPercentDesc.Text = "0";
            this.txtPercentDesc.TextChanged += new System.EventHandler(this.txtPercentDesc_TextChanged);
            // 
            // txtDspDocLessDiscount
            // 
            this.txtDspDocLessDiscount.Location = new System.Drawing.Point(634, 2);
            this.txtDspDocLessDiscount.Name = "txtDspDocLessDiscount";
            this.txtDspDocLessDiscount.Size = new System.Drawing.Size(70, 20);
            this.txtDspDocLessDiscount.TabIndex = 4;
            this.txtDspDocLessDiscount.Text = "0";
            this.txtDspDocLessDiscount.TextChanged += new System.EventHandler(this.txtDspDocLessDiscount_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(529, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "% Desc";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(618, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "$";
            // 
            // InvoiceLine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtDspDocLessDiscount);
            this.Controls.Add(this.txtPercentDesc);
            this.Controls.Add(this.chkDescPercent);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtTaxAmt);
            this.Controls.Add(this.cmbImpuesto);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDspDocExtPrice);
            this.Controls.Add(this.txtCantidad);
            this.Controls.Add(this.txtUnitPrice);
            this.Controls.Add(this.txtPartNumPartDescription);
            this.Controls.Add(this.lblInvoiceLine);
            this.Name = "InvoiceLine";
            this.Size = new System.Drawing.Size(988, 24);
            this.Load += new System.EventHandler(this.InvoiceLine_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInvoiceLine;
        private System.Windows.Forms.TextBox txtPartNumPartDescription;
        private System.Windows.Forms.TextBox txtUnitPrice;
        private System.Windows.Forms.TextBox txtCantidad;
        private System.Windows.Forms.TextBox txtDspDocExtPrice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbImpuesto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTaxAmt;
        private System.Windows.Forms.CheckBox chkDescPercent;
        private System.Windows.Forms.TextBox txtPercentDesc;
        private System.Windows.Forms.TextBox txtDspDocLessDiscount;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}
